<?php
class emp_medical_model extends CI_Model{

	function getEmpMedRecord(){
	
	
		$q=$this->db->get('medicalrecord');
		return $q->result();
		
	
	
	}
	function getEmpMedById(){
		$this->db->where('eid',$this->input->post('id'));
		$q=$this->db->get('medicalrecord');
		return $q->result();
	
	
	}
	function addEmpMed($data){
	
		$q=$this->db->insert('medicalrecord',$data);
		if($q){
			return true;
		
		}
		else{
			return false;
		
		
		}
	
	}
	function deleteEmpMed(){
		$this->db->where('eid',$this->input->post('id'));
		$q=$this->db->delete('medicalrecord');
		if($q){
			return true;
		}
		else{
			return false;
		}
	
	
	
	
	}
	function updateEmpMed($data){
		$this->db->where('eid',$this->input->post('id'));
		$this->db->update('medicalrecord',$data);
		
	}




}