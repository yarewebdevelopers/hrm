<?php
class emp_award_model extends CI_Model{
	function getEmpAwards(){
	
		$q=$this->db->get('awards');
		return $q->result();
	
	
	}
	function getEmpAwardsById(){
		$this->db->where('eid',$this->input->post('id'));
		$q=$this->db->get('awards');
		return $q->result();
		
		
	
	
	}
	function addEmpAwards($data){
		$q=$this->db->insert('awards',$data);
		if($q){
			return true;
		
		}
		else{
			return false;
		
		}
		
	
	
	
	}
	function deleteEmpAwards(){
		$this->db->where('eid',$this->input->post('id'));
		$q=$this->db->delete('awards');
		if($q){
			return true;
		
		}
		else{
			return false;
		}
	
	}
	function updateEmpAwards($data){
		$this->db->where('eid',$this->input->post('id'));
		$q=$this->db->update('awards',$data);
		if($q){
		
			return true;
		
		}
		else{
			return false;
		}
	
	
	
	}



}