<?php
class emp_attach_model extends CI_Model{

	function getEmpAttachments(){
	
		$q=$this->db->get('attachment');
		return $q->result();
		
	
	
	
	}
	function getEmpAttById(){
		$this->db->where('eid',$this->input->post('id'));
		$q=$this->db->get('attachment');
		return $q->result();
	
	
	}
	function addEmpAtt($data){
		$q=$this->db->insert('attachment',$data);
	    if($q){
	    	return true;
	    
	    
	    }
	    else{return false;}
	}
	
    function deleteEmpAtt(){
    	$this->db->where('eid',$this->input->post('id'));
    	$q=$this->db->delete('attachment');
    	
    	if($q){
    		return true;
    	
    	
    	}
    	else{
    		return false;
    	
    	}
    
    
    
    }
    function updateEmpAtt($data){
    	$this->db->where('eid',$this->input->post('id'));
    	$q=$this->db->update('attachment',$data);
    	if($q){
    		return true;
    	
    	}
    	else{
    	
    		return false;
    	
    	
    	}
    
    
    
    
    }
    


}