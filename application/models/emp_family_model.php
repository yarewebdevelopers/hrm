<?php
class emp_family_model extends CI_Model{
function get_emp_family_record($id){
$this->db->select('family.id,family.famname,relation.relation,family.age,family.status,family.profession,family.permaddress,family.contactno,family.isnok,family.distr')->from('family,relation');
$this->db->where('family.status',1);
$this->db->where('family.eid',$id);
$this->db->where('family.relationid=relation.id');

$query=$this->db->get();
if($query->num_rows()>0){
$result=$query->result();
return $result;
}
else{
return false;
}

}


function get_emp_personal_record($id){
$this->db->select('employee.id,employee.no,employee.name,rank.rank,department.depname')->from('employee,rank,department');
$this->db->where('employee.id',$id);
$this->db->where('employee.depid=department.depid');
$this->db->where('employee.rankid=rank.rankid');
$query=$this->db->get();
if($query->num_rows()>0){
$result=$query->result();
//var_dump($result);exit;
return $result;
}
else{
return false;
}

}


function add_emp_family_record($id){
if($this->input->post('checknok')!=null){
$nok=$this->input->post('checknok');
}
else{
$nok="no";
}

$data=array(
'famname'=>$this->input->post('name'),
'relationid'=>$this->input->post('relation'),
'age'=>$this->input->post('age'),
'status'=>$this->input->post('status'),
'profession'=>$this->input->post('profession'),
'permaddress'=>$this->input->post('address'),
'city'=>$this->input->post('city'),
'contactno'=>$this->input->post('phone_code').$this->input->post('p_number'),
'isnok'=>$nok,
'distr'=>$this->input->post('distr'),
'eid'=>$id


);
$this->db->insert('family',$data);
return true;

}

function get_emp_fam_record_for_edit($id){
$this->db->select('family.id,family.isalive,family.famname,family.relationid,relation.relation,family.age,family.status,family.profession,family.permaddress,family.city,family.contactno,family.isnok,family.distr,family.eid')->from('family,relation');

$this->db->where('family.id',$id);
$this->db->where('family.relationid=relation.id');
$query=$this->db->get();
if($query->num_rows()>0){
$result=$query->result();
return $result;
}
else{
return false;
}
 	}
	///////////////////////////////////////////////////////////////////
	function get_emp_fam_record_deleted($id){
	$this->db->select('family.id,family.famname,relation.relation,family.age,family.status,family.profession,family.permaddress,family.contactno,family.isnok,family.distr')->from('family,relation');
$this->db->where('family.status',0);
$this->db->where('family.eid',$id);
$this->db->where('family.relationid=relation.id');

$query=$this->db->get();
if($query->num_rows()>0){
$result=$query->result();
return $result;
}
else{
return false;
}
	}
	
	
	
	
	
 	function get_emp_detail($id){
	$this->db->select('employee.id,employee.no,employee.name,rank.rank,department.depname')->from('employee,rank,department');
$this->db->where('employee.id',$id);
$this->db->where('employee.depid=department.depid');
$this->db->where('employee.rankid=rank.rankid');
$query=$this->db->get();
if($query->num_rows()>0){
$result=$query->result();
//var_dump($result);exit;
return $result;
}
else{
return false;
}
	
	
	
	}
	
 
 	
	function del_emp_fam_record($id){
	$data=array(
	'status'=>0
	);
	$this->db->where('id',$id);
	$this->db->update('family',$data);
	$this->db->select('family.eid')->where('family.id',$id);
	$result=$this->db->get('family');
	foreach($result->result() as $res){
	$data=$res->eid;
	}
	return $data;
	
	}
	
	
 function update_emp_family_record($id){
 
 if($this->input->post('checknok')!=null){
$nok=$this->input->post('checknok');
}
else{
$nok="no";
}

$data=array(
'famname'=>$this->input->post('name'),
'relationid'=>$this->input->post('relation'),
'age'=>$this->input->post('age'),
'isalive'=>$this->input->post('status'),
'profession'=>$this->input->post('profession'),
'permaddress'=>$this->input->post('address'),
'city'=>$this->input->post('city'),
'contactno'=>$this->input->post('phone_code').$this->input->post('p_number'),
'isnok'=>$nok,
'distr'=>$this->input->post('distr')


);
//print_r($data);exit;
//print_r($data);exit;
$this->db->where('id',$id);
$this->db->update('family',$data);
$this->db->select('family.eid')->where('family.id',$id);
	$result=$this->db->get('family');
	foreach($result->result() as $res){
	$data=$res->eid;
	}
	return $data;
	
	}
	
	function recover_deleted_record($id){
	
	$data=array(
	'status'=>1
	);
	$this->db->where('id',$id);
	$this->db->update('family',$data);
	$this->db->select('family.eid')->where('family.id',$id);
	$result=$this->db->get('family');
	foreach($result->result() as $res){
	$data=$res->eid;
	}
	return $data;
	
	
	}

 
 
 



}