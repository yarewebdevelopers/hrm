<?php
class emp_model extends CI_Model{
	function tableGetEmp(){
	    $this->db->from('employee,rank,cast');
 	
 		$this->db->where('employee.status',1);
 		$this->db->where('employee.rankid=rank.rankid');

 		$this->db->where('employee.castid=cast.castid');

 		$q=$this->db->get();
 	if($q->num_rows()>0){
 		return $q->result();
 	
 	}
	
	}
	function getDeletedEmp(){
		    $this->db->from('employee,rank,cast');
 	
 		$this->db->where('employee.status',0);
 		$this->db->where('employee.rankid=rank.rankid');

 		$this->db->where('employee.castid=cast.castid');

 		$q=$this->db->get();
 	if($q->num_rows()>0){
 		return $q->result();
 	
 	}
		
		
	
	
	}
	function tableGetEmpById(){
	    $this->db->from('employee,rank,department,cast');
	    $this->db->where('employee.id',$this->uri->segment(3));
 	
 		$this->db->where('employee.rankid=rank.rankid');

 		$this->db->where('employee.depid=department.depid');
 		$this->db->where('employee.castid=cast.castid');

 		$q=$this->db->get();
 	if($q->num_rows()>0){
 		return $q->result();
 	
 	}
	
	}
	
	function tableGet($table){
	$q=$this->db->get($table);
	if($q->num_rows()>0){
	return $q->result();
	}
	
	
	}


	function tableGetById($table){
		if($this->uri->segment(4)!=NULL){
			        $this->db->where('eid',$this->uri->segment(3));
                        $this->db->where('id',$this->uri->segment(4));
                        $this->db->where('status',1);
		}
		else if($this->uri->segment(3)!=NULL){
                        $this->db->where('eid',$this->uri->segment(3));
                        $this->db->where('status',1);	
                }
                else{
		$this->db->where('eid',$this->input->post('id'));
                $this->db->where('status',1);
                }


		$q=$this->db->get($table);
		if($q->num_rows()>0){
			
		return $q->result();
		}
		
	
	}
	function getAttP($id){
		
		$this->db->where('id',$id);
		$q=$this->db->get('attachment');
		if($q->num_rows()>0){
			return $q->result();
		
		}
		
		
	
	
	
	}
	
	
	function addRecord($table,$data){
		
	$q=$this->db->insert($table,$data);
	if($q){
		return true;
	
	
	
	}
	else{
		return false;
	}
	
	
	
	}

	function deleteTableRecord($table)
        {
            if($this->uri->segment(4)!=NULL){
                        $this->db->where('id',$this->uri->segment(4));
                        $data=array('status'=>0);
                        
		}
		else if($this->input->post('id'))
            {
		$this->db->where('id',$this->input->post('id'));
            }
       else if($this->uri->segment(3))
            {
            	$data=array('status'=>0);
            	
                $this->db->where('id',$this->uri->segment(3));
            }
	
            $q=$this->db->update($table,$data);
	
            if($q)
            {
                return true;
            }
            else
            {
                return false;
            }

	}
	function updateTableRecord($table,$data){
		
		//echo $this->uri->segment(3);exit;
            if($this->uri->segment(4)!=NULL)
            {
                $this->db->where('id',$this->uri->segment(4));
            }
            else
            {
		$this->db->where('id',$this->uri->segment(3));
            }
            $q=$this->db->update($table,$data);

            if($q)
            {
                return true;
            }
            else
            {
                return false;
            }
	
	}
	function updateAtt($table,$data){
		$this->db->where('id',$this->uri->segment(3));
		$q=$this->db->update($table,$data);
             
		if($q){
			return true;
		}
		else{
			return false;
		}
	}
	function getCities(){
	
		$this->db->where('CountryCode','PAK');
		$q=$this->db->get('city');
		return $q->result();
	
	}
	function getDeletedRecords($id,$table){
		$this->db->where('eid',$id);
		$this->db->where('status',0);
		$q=$this->db->get($table);
		if($q->num_rows()>0){
			return $q->result();
		
		
		
		}
	
	
	}
        
        function getDeleted($table)
        {
            $this->db->where('status',0);
            $this->db->where('eid='.$this->uri->segment(3));
            $q = $this->db->get($table);
            if($q->num_rows()>0)
            {
                return $q->result();
            }
        }
        
}


