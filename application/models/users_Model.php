<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of newPHPClass
 *
 * @author ali00
 */
class  Users_Model extends CI_Model
{
    
    
   public  function __construct() 
    {
        parent::__construct();
    }
  
	//////////////check_login function////////////
	public function check_login(){
		//checking login and creating session
            $this->db->select('name')
                    ->select('email')
                    ->Select('password');
	$this->db->where('email',$this->input->post('email'));

	$this->db->where('password',MD5($this->input->post('password')));
	
	$result = $this->db->get('users');
	 if($result->num_rows()>0)
        {
        		
				foreach($result->result() as $rows)
					{
						//add all data to session
						$newdata = array(
                                                                'name' =>$rows->name,
								'user_email'    => $rows->email,
								'user_password'=>$rows->password,
								'logged_in'  => TRUE
										);
					}
					$this->session->set_userdata($newdata);
					$data = array(
					'flag' => 1
					);
					$this->db->update('users',$data);
					
					//echo $this->session->userdata('user_email');
            
           return true;
            
        }
		else{
				return false;
			}
	
	}//
	//////////////////////logout_flag////////////////////////
	public function logout_flag(){
	
					$data = array(
					'flag' => 0
					);
					$this->db->where("email",$this->session->userdata('user_email'));
					$this->db->update('users',$data);
					
	
	}
	////////////check user already exists in database while updating///////////////////
	public function check_user(){
	$this->db->where('email',$this->input->post('email'));
	$check=$this->db->get('users');
	if($check->num_rows()>0){
	return false;
	
	}
	else{
	return true;
	}
	}
	///////////////////////////
	/////////////////////////create user///////////////////////////////
	public function create_user(){
	$this->db->where('email',$this->input->post('email'));
	$check=$this->db->get('users');
	if($check->num_rows()>0){
	return false;
	
	}
	else{//else 22
	$data=array(
		'name'=>$this->input->post('name'),
		'email'=>$this->input->post('email'),
		'password'=>md5($this->input->post('password')),
		'roleid'=>$this->input->post('role'),
		'depid'=>$this->input->post('office'),
		'status'=>1
	);
	$this->db->insert('users',$data);
	return true;
	}//else22
	}
	////////////////////////delete user/////////////////////
	public function delete_user($id){
	//var_dump($id);exit;
	$data=array(
		'status'=>0,
		
	);
	$this->db->where('id',$id);
	$this->db->update('users',$data);
	return true;
	}
	///////////////////////////update_user//////////
	public function update_user($id){
	//echo $id;exit;
	$data=array(
		'name'=>$this->input->post('name'),
		'email'=>$this->input->post('email'),
		'password'=>md5($this->input->post('password')),
		'roleid'=>$this->input->post('role'),
		'depid'=>$this->input->post('office')
	);
	$this->db->where('id',$id);
	$this->db->update('users',$data);
	}
	///////////////////////////update_usuperadmin//////////
	public function update_superadmin($id){
	//echo $id;exit;
	$data=array(
		'name'=>$this->input->post('name'),
		'email'=>$this->input->post('email'),
		'password'=>$this->input->post('password'),
		
	);
	$this->db->where('id',$id);
	$this->db->update('users',$data);
	}
	////////////////////////show_users//////////////////////
	public function show_user(){


  $this->db->select('users.id,users.name,users.email,role.role,department.depname')->from('users,role,department');
		$this->db->where('users.status',1);
	 $this->db->where('users.roleid=role.id');
	 $this->db->where('users.depid=department.depid');
	 $query=$this->db->get();
	 
     $result=$query->result();
     	//print_r($result);exit;
	
		return $result;
								 
	
	}
	/////////////show deleted users//////////////
	public function show_users_deleted(){


  $this->db->select('users.id,users.name,users.email,role.role,department.depname')->from('users,role,department');
		$this->db->where('users.status',0);
	 $this->db->where('users.roleid=role.id');
	 $this->db->where('users.depid=department.depid');
	 $query=$this->db->get();
	 
    $result=$query->result();
	//print_r($result);exit;
	
								 return $result;
								 
	
	}
	/////////////////////////////return data for into edit pages input for editing info of particular person//////////////////////////////
	///////////////////////////edit_user_data/////////////////////////////////
	public function edit_user_data($id){
	
	$this->db->where('id',$id);
		$query=$this->db->get('users');
		//echo $id;exit;
	  $result=$query->result_array();
	//print_r($result);exit;
	
								 return $result;
	}
	///////////////////////////////enable deleted users////////////////////////////////////////
	public function enable_deleted_users($id){
	$data=array(
		'status'=>1,
		
	);
	$this->db->where('id',$id);
	$this->db->update('users',$data);
	return true;
	}
}//class ending bracket
