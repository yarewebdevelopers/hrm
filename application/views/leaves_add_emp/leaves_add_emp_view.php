<div id="page-wrapper">

<?php// print_r($records);exit;?>
    <div class="page-content">

        <!-- begin PAGE TITLE ROW -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>Staff Management
                        
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url();?>/emp_emp_cont">Staff Management</a>
                        </li>
                        <li class="active">Add Leaves</li>
                    </ol>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>


       <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Add Leaves Form</h4>
                        </div>
                        <div class="clearfix">
                            
                        </div>
                    </div>
                      <div class="error">
                    <?php if($this->uri->segment(4)){?>
                    	
                    	
                    	
                    	
                    	
                    
                        
                     <div class="alert alert-success" role="alert">
                                        <span class="fa fa-thumbs-up" aria-hidden="true"></span>
                                        <span class="sr-only">Message:</span>
                                        <?php echo "Leave Record Added Successfully";
 
                                        
                                        ?>
                                                                   
                                       
                                        
                                    </div>
                    <?php }?>
                    
                    </div>
                    
                    
                    
                    
                    
                    <div class="panel-collapse collapse in" id="validationExamples">
                        <div class="portlet-body">
                            <form class="form-horizontal" method="post" action="<?php echo base_url('emp_leave_cont/AddLeave')?>" id="validate" role="form" novalidate="novalidate">
                                
                                <!-- Start of father's name name and number fields -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="number">Number</label>
                                    <div class="col-sm-4">
                                        <input value="<?php echo $records[0]->no;?>" name="number" class="form-control" id="number" disabled="" type="text"/>
                                    </div>
                                
                                    <label class="col-sm-2 control-label" for="name">Name</label>
                                    <div class="col-sm-4">
                                        <input value="<?php echo $records[0]->name;?>" name="name" class="form-control" id="name" disabled="" type="text"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="number">Rank</label>
                                    <div class="col-sm-4">
                                        <input value="<?php echo $records[0]->rank;?>" name="rank" class="form-control" id="rank" disabled="" type="text"/>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="from">From</label>
                                    <div class="col-sm-4" id="sandbox-container">
                                        <input name="from" class="form-control" id="from" required="" type="text"/>
                                    </div>
                                    <label class="col-sm-2 control-label" for="name">To</label>
                                    <div class="col-sm-4" id="sandbox-container">
                                        <input name="to" class="form-control" id="to" required="" type="text"/>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="reason">Reason</label>
                                    <div class="col-sm-4">
                                        <input name="reason" class="form-control" id="reason" required="" type="text"/>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="remarks">Remarks</label>
                                    <div class="col-sm-4">
                                        <textarea name="remarks" class="form-control" id="remarks" type="text" rows="3"></textarea>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                    <?php //print_r($records);exit;?>
                                    <input type="hidden" id="eid" name="eid" value="<?php echo $records[0]->id;?>" />
                                        <input class="btn btn-default" type="submit" id="submit" name="submit" value="Add Leaves"/>
                                        
                                         <input class="btn btn-default" type="reset" value="Cancel"/>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
       </div>
        <!-- /.col-lg-12 -->
        <!-- End Validation States -->
    </div>
</div>
