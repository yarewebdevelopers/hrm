<?php

    if(isset($this->session->userdata['logged_in'])):
        $this->load->view('includes/header');

        $this->load->view('includes/menu_bar_top');

        $this->load->view($main_page.'/'.$main_page.'_view');
        $this->load->view('includes/footer');
    else:
        
        redirect('login');    
        
    endif;
    