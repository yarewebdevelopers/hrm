<!-- begin SIDE NAVIGATION -->
        <nav class="navbar-side" role="navigation">
            <div class="navbar-collapse sidebar-collapse collapse">
                <ul id="side" class="nav navbar-nav side-nav">
                    <!-- begin SIDE NAV USER PANEL -->
                    <li class="side-user hidden-xs">
                        <img class="img-circle" src="<?php echo base_url();?>public/img/profile-pic.jpg" alt="">
                        <p class="welcome">
                            <i class="fa fa-key"></i> Logged in as
                        </p>
                        <p class="name tooltip-sidebar-logout">
                            <span class="last-name"></span> <a style="color: inherit" class="logout_open" href="#logout" data-toggle="tooltip" data-placement="top" title="Logout"><i class="fa fa-sign-out"></i></a>
                        </p>
                        <div class="clearfix"></div>
                    </li>
                    <!-- end SIDE NAV USER PANEL -->
                    <!-- begin SIDE NAV SEARCH -->
                    <li class="nav-search">
                        <form role="form">
                            <input type="search" class="form-control" placeholder="Search...">
                            <button class="btn">
                                <i class="fa fa-search"></i>
                            </button>
                        </form>
                    </li>
                    <!-- end SIDE NAV SEARCH -->
                    <!-- begin DASHBOARD LINK -->
                    <li>
                        <a class="active" href="<?php echo base_url();?>welcome">
                            <i class="fa fa-dashboard"></i> Dashboard
                        </a>
                    </li>
                    <!-- end DASHBOARD LINK -->
                    <!-- begin CHARTS DROPDOWN -->
                    <li class="panel">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#charts">
                            <i class="fa fa-bar-chart-o"></i> Setup Entries <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="charts">
                            <li>
                                <a href="<?php echo base_url();?>welcome/departments">
                                    <i class="fa fa-angle-double-right"></i> Departments
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url();?>welcome/caste">
                                    <i class="fa fa-angle-double-right"></i> Caste
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url();?>welcome/csd">
                                    <i class="fa fa-angle-double-right"></i> CSD Items
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url();?>welcome/awards">
                                    <i class="fa fa-angle-double-right"></i> Awards
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- end SETUP Entries DROPDOWN -->
                    <!-- begin FORMS DROPDOWN -->
                    
                    <!-- begin Users LINK -->
                    <li>
                        <a href="<?php echo base_url();?>welcome/users">
                            <i class="fa fa-user"></i> Setup Entries
                        </a>
                    </li>
                    <!-- end Users LINK -->
                    
                    <li class="panel">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#forms">
                            <i class="fa fa-edit"></i> Employee Management <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="forms">
                            <li>
                                <a href="<?php echo base_url();?>welcome/employee_add">
                                    <i class="fa fa-angle-double-right"></i> Add Employee
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url();?>welcome/employee_view">
                                    <i class="fa fa-angle-double-right"></i> Employee Directory
                                </a>
                            </li>
                            
                        </ul>
                    </li>
                    <!-- end FORMS DROPDOWN -->
                    <!-- begin TABLES DROPDOWN -->
                    <li class="panel">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#tables">
                            <i class="fa fa-bar-chart-o"></i> Statistics <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="collapse nav" id="tables">
                            <li>
                                <a href="<?php echo base_url();?>welcome/attachments">
                                    <i class="fa fa-angle-double-right"></i> Attachments
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url();?>welcome/classes">
                                    <i class="fa fa-angle-double-right"></i> Classes
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url();?>welcome/instalments">
                                    <i class="fa fa-angle-double-right"></i> CSD Installments
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url();?>welcome/inquires">
                                    <i class="fa fa-angle-double-right"></i> Inquiries
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url();?>welcome/leaves">
                                    <i class="fa fa-angle-double-right"></i> Leaves
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url();?>welcome/promotions">
                                    <i class="fa fa-angle-double-right"></i> Promotions
                                </a>
                            </li>
                            <li>
                                <a href="<?php echo base_url();?>welcome/uniforms">
                                    <i class="fa fa-angle-double-right"></i> Uniforms
                                </a>
                            </li>
                        </ul>
                    </li>
                    <!-- end TABLES DROPDOWN -->
                    <!-- begin UI ELEMENTS DROPDOWN -->
                    <li class="panel">
                        <a href="javascript:;" data-parent="#side" data-toggle="collapse" class="accordion-toggle" data-target="#ui-elements">
                            <i class="fa fa-wrench"></i> Reports <i class="fa fa-caret-down"></i>
                        </a>
                    </li>
                        
                    
                    <li class="panel">
                        <a class="logout_open" title="" data-placement="top" data-toggle="tooltip" href="#logout" data-popup-ordinal="1" data-original-title="Logout">
                        
                            <i class="fa fa-sign-out"></i>
                            Logout
                            
                        </a>
                    </li>
                </ul>
                <!-- /.side-nav -->
            </div>
            <!-- /.navbar-collapse -->
        </nav>

<!-- /.navbar-side -->
        <!-- end SIDE NAVIGATION -->

