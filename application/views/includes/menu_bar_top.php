<div id="wrapper">

        <!-- begin TOP NAVIGATION -->
        <nav class="navbar-top" role="navigation">

            <!-- begin BRAND HEADING -->
            <div class="navbar-header">
                
                <div class="navbar-brand">
                    <a href="index.html">
                        <img src="<?php echo base_url();?>public/img/flex-admin-logo.png" data-1x="<?php echo base_url();?>public/img/flex-admin-logo@1x.png" data-2x="<?php echo base_url();?>public/img/flex-admin-logo@2x.png" class="hisrc img-responsive" alt="">
                    </a>
                </div>
            </div>
            <!-- end BRAND HEADING -->

            <div class="nav-top">

                <!-- begin MESSAGES/ALERTS/TASKS/USER ACTIONS DROPDOWNS -->
                <ul class="nav navbar-right">
				<li class="dropdown">
                        <a href="<?php echo base_url();?>welcome">
                            <i class="fa fa-dashboard"> Dashboard</i>
                        </a>
				</li>

			<li class="dropdown">
                        <a href="#" data-toggle="dropdown">
                            <i class="fa fa-wrench"> Setup Entries</i> <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-scroll dropdown-messages">                                                      
                            <ul class="list-unstyled">
                                
                                <li>
                                    <a href="<?php echo base_url();?>emp_award_cont">
                                        <i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Awards
                                    </a>
                                    
                                </li>
                                
                                <li>
                                    <a href="<?php echo base_url();?>emp_cast_cont">
                                        <i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Caste
                                    </a>
                                </li>
                                
                                <li>
                                    <a href="<?php echo base_url();?>emp_csd_cont">
                                        <i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CSD Items
                                    </a>
                                </li>
                                
                                <li>
                                    <a href="<?php echo base_url();?>emp_dpt_cont">
                                        <i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Departments
                                    </a>
                                </li>
                                
                            </ul>
                        </ul>
                    </li>
				    <!-- begin Staff DROPDOWN -->
                    <li class="dropdown">
                        <a href="<?php echo base_url();?>emp_emp_cont" data-toggle="dropdown">
                            <i class="fa fa-user"> Staff</i> <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-scroll dropdown-messages">                                                      
                            <ul class="list-unstyled">
                                <li>
                                        <a href="<?php echo base_url("emp_emp_cont/addEmp");?>"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Add Staff</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url();?>emp_emp_cont"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Staff Directory</a>
                                </li>
                                <li>
                                    <a href="<?php echo base_url();?>user_controller/load_userview_page/users"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;User Management</a>
                                </li>
                            </ul>
                        </ul>
                    </li>
                    <!-- /.dropdown -->
					<!-- begin Statistics DROPDOWN -->
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown">
                            <i class="fa fa-signal"> Statistics</i> <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-scroll dropdown-messages">                                                      
                            <ul class="list-unstyled">
                                <li>
                                        <a href="<?php echo base_url();?>emp_att_cont"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Attachments</a>
                                </li>
                                <li>
                                        <a href="<?php echo base_url();?>emp_lacking_cont"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Classes</a>
                                </li>
                                <li>
                                        <a href="<?php echo base_url();?>emp_csd_cont"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;CSD Installments</a>
                                </li>
                                <li>
                                        <a href="<?php echo base_url();?>emp_punish_cont/getPRecordById"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Inquiries</a>
                                </li>
                                <li>
                                        <a href="<?php echo base_url();?>emp_leave_cont/getLeaveRecord"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Leaves</a>
                                </li>
                                <li>
                                        <a href="<?php echo base_url();?>welcome/promotions"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Promotions</a>
                                </li>
                                <li>
                                        <a href="<?php echo base_url();?>emp_uniform_cont"><i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Uniforms</a>
                                </li>
                            </ul>
                        </ul>
                    </li>
                    <!-- /.dropdown -->
					<!-- begin Reports DROPDOWN -->
                    <li class="dropdown">
                        <a href="#" data-toggle="dropdown">
                            <i class="fa fa-file"> Reports</i> <i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-scroll dropdown-messages">                                                      
							<ul class="list-unstyled">
								<li>
									<a href="report.php">Report 1</a>
								</li>
								
							</ul>
						</ul>
                    </li>
                    <!-- /.dropdown -->
                   
                    <!-- begin USER ACTIONS DROPDOWN -->
                    <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                            <i class="fa fa-user"></i><i class="fa fa-caret-down"></i>
                        </a>
                        <ul class="dropdown-menu dropdown-user">
                            <li>
                                <a href="profile.html">
                                    <i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;Change Password
                                </a>
                            </li>
                            
                            <li>
                                <a class="logout_open" href="#logout">
                                    <i class="fa fa-angle-double-right"></i>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                                    Logout
                                    <strong><?php $result = $this->session->userdata('name');
                                                            echo ucfirst($result);?></strong>
                                </a>
                            </li>
                        </ul>
                        <!-- /.dropdown-menu -->
                    </li>
                    <!-- /.dropdown -->
                    <!-- end USER ACTIONS DROPDOWN -->
                </ul>

            </div>
            <!-- /.nav-top -->
        </nav>
        <!-- /.navbar-top -->
        <!-- end TOP NAVIGATION -->
</div>