<div id="page-wrapper">

<div class="page-content page-content-ease-in">

    <!-- begin PAGE TITLE ROW -->
    <div class="row">
        <div class="col-lg-12">
            <div class="page-title">
                <h1>Staff Management
                    <a href="<?php echo base_url('user_controller/load_pages/users_add')?>"><input class="btn btn-default" type="submit" value="Add User"/></a>
					<a href="<?php echo base_url('user_controller/load_deleted_user_page/deleted_user')?>"><input class="btn btn-danger" type="submit" value="Trash"/></a>
                </h1>
                <ol class="breadcrumb">
                    <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url('welcome/');?>">Dashboard</a>
                    </li>
                    <li class="active">User Management</li>
					
                </ol>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- end PAGE TITLE ROW -->

    <!-- begin ADVANCED TABLES ROW -->
    <div class="row">

        <div class="col-lg-12">

            <div class="portlet portlet-default">
                <div class="portlet-heading">
                    <div class="portlet-title">
                        <h4>User Directory</h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <div role="grid" class="dataTables_wrapper form-inline" id="example-table_wrapper">

                            <table class="table table-striped table-bordered table-hover table-green dataTable" id="example-table" aria-describedby="example-table_info">

                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width:auto;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                            Sr.#
                                        </th>
                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Browser: activate to sort column ascending">
                                            Name
                                        </th>
                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Platform(s): activate to sort column ascending">
                                            Email
                                        </th>
										<th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Engine version: activate to sort column ascending">
                                            Role
                                        </th>
                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Engine version: activate to sort column ascending">
                                            Department
                                        </th>
                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="CSS grade: activate to sort column ascending">
                                            Actions
                                        </th>
                                    </tr>
                                </thead>
								

                                <tbody role="alert" aria-live="polite" aria-relevant="all">
								<?php
							if(isset($users_data)){//if id1
							$count=1;
								foreach($users_data as $users){
								if($users->role=="superadmin") {?>
                                    <tr class="gradeA odd">
                                        <td class="  sorting_1"><?php echo $count;?></td>
                                        <td class=" "><?php echo $users->name;?></td>
                                        <td class=" "><?php echo $users->email;?></td>
										<td class=" "><?php echo $users->role;?></td>
                                        <td class="center "><?php echo $users->depname;?></td>
                                        <td class="center ">
                                            <a href="<?php echo base_url('index.php/user_controller/load_edit_superadmin_page/'.$users->id);?>"><li class="fa fa-edit"></li></a>
                                            
                                            <!--<li class="fa fa-trash-o"></li>-->
                                           
                                        </td>
                                    </tr>
								<?php	}//ending if
									else{?>
									  <tr class="gradeA odd">
                                        <td class="  sorting_1"><?php echo $count;?></td>
                                        <td class=" "><?php echo $users->name;?></td>
                                        <td class=" "><?php echo $users->email;?></td>
										<td class=" "><?php echo $users->role;?></td>
                                        <td class="center "><?php echo $users->depname;?></td>
                                        <td class="center ">
                                            <a href="<?php echo base_url('index.php/user_controller/load_update_page/'.$users->id);?>"><li class="fa fa-edit"></li></a>
                                            
                                         <a href="<?php echo base_url('index.php/user_controller/delete_user/'.$users->id);?>">   <li class="fa fa-trash-o"></li></a>
                                           
                                        </td>
                                    </tr>
									
								<?php	}
								$count++;
									}//ending foreach
									}//if id1
									else{
									echo "no users exists";
									}?>
                                   
                                    
                                   
                                   
                            </tbody>
                        </table>

                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.portlet-body -->
            </div>
            <!-- /.portlet -->

        </div>
        <!-- /.col-lg-12 -->

    </div>
    <!-- /.row -->

</div>
<!-- /.page-content -->

</div>