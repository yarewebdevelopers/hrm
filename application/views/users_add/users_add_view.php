<div id="page-wrapper">


    <div class="page-content">

        <!-- begin PAGE TITLE ROW -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>User Management
					<a href="<?php echo base_url('/user_controller/load_userview_page/users')?>"><input class="btn btn-default" type="submit" value="back"/></a>
                        
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i>  <a href="index.html">User Management</a>
                        </li>
                        <li class="active">Add User</li>
                    </ol>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>


       <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Add User Form</h4>
                        </div>
                        <div class="clearfix">
                            
                        </div>
                    </div>
                    <div class="panel-collapse collapse in" id="validationExamples">
                        <div class="portlet-body">
                        <form class="form-horizontal" id="validate" role="form" method="post" novalidate="novalidate" action="<?php echo base_url('/user_controller/create_user');?>">
                             <!--<?php echo form_open(base_url('index.php/user_controller/create_user'));?>-->
                                <!-- Start of father's name name and number fields -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="number">Email</label>
                                    <div class="col-sm-4">
                                        <input name="email" class="form-control" id="number"  required="" type="email" placeholder="It is Required"/>
                                    </div>
                                
                                    <label class="col-sm-2 control-label" for="name">Name</label>
                                    <div class="col-sm-4">
                                        <input name="name" class="form-control" id="name" required=""  type="text" placeholder="It is Required"/>
                                    </div>
                                </div>
                                
                                <div class='form-group'>                              
                                    <label class="col-sm-2 control-label" for="password" >Password</label>
                                    <div class="col-sm-4">
                                        <input name="password" class="form-control" id="password" type="password" required="" placeholder="It is Required"/>
                                    </div>
                                
                                    
                                    <label class="col-sm-2 control-label" for="cnic">Confirm Password</label>
                                    <div class="col-sm-4">
                                        <input name="c_password" class="form-control" id="c_password" required="" type="password" value="" placeholder="It is Required"/>
                                    </div>
                                </div>  
                                        
                                <div class="form-group">  
                                    <label class="col-sm-2 control-label">Role</label>
                                    <div class="col-sm-4">
                                        <select name="role" class="form-control" required="">
                                            <option value="">Select One:</option>
                                            <option value="2">Admin</option>
                                            <option value="3">employee</option>
                                            
                                        </select>
										
                                    </div>
                                
                                    
                                    
                                    <label class="col-sm-2 control-label" for="reporting">Department</label>
                                    <div class="col-sm-4">
                                        <select name="office" class="form-control" required="">
											 <option value="">Select One:</option>
                                            <option value="1">P btry</option>
                                            <option value="2">Q btry</option>
                                            <option value="3">R btry</option>
                                            <option value="4">HQ Btry</option>
                                        </select>
                                            
                                    </div>
                                </div> 
                                <!-- end of CNIC, rank and reporting to-->
                                    
                                
                                                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <input class="btn btn-default" type="submit" value="Add User"/>
                                        
                                  </button><input class="btn btn-default" type="reset" value="Cancel" />
                                    </div>
                                    
                                </div>
                            </form>
							           <?php echo validation_errors();?>
		
			 <?php
                        if(isset($error)){
						echo "<br/>".$error;
						}
						else if(isset($success)){
						echo "<br/>".$success;
						}
						
			?>
                        </div>
                    </div>
                </div>
            </div>
       </div>
        <!-- /.col-lg-12 -->
        <!-- End Validation States -->
    </div>
</div>

