<div id="page-wrapper">


    <div class="page-content">

        <!-- begin PAGE TITLE ROW -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>Staff Family Management
                        
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url('emp_family_cont/get_emp_family_record/'.$records[0]->eid);?>">Staff Management</a>
                        </li>
                        <li class="active">Edit Staff Family</li>
                    </ol>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>


       <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Edit Staff Family Form</h4>
                        </div>
                        <div class="clearfix">
                            
                        </div>
                    </div>
                    <div class="panel-collapse collapse in" id="validationExamples">
                        <div class="portlet-body">
                            <form class="form-horizontal" id="validate" role="form" novalidate="novalidate" action="<?php echo base_url('emp_family_cont/update_emp_family_record/'.$records[0]->id);?>" method="post">
                                
								<?php if(isset($records)){
								foreach($records as $rec){
								//var_dump($rec);exit;
								?>
                                <!-- Start of father's name name and number fields -->
                                <div class="form-group">
                                                                  
                                    <label class="col-sm-2 control-label" for="name">Name</label>
                                    <div class="col-sm-4">
                                        <input name="name" class="form-control" id="name" required="" type="text" value="<?php echo $rec->famname;?>" placeholder="require"/>
                                    </div>
                                    <label class="col-sm-2 control-label" for="relation">Relation</label>
                                    <div class="col-sm-4">
                                        <select name="relation" class="form-control" id="relation" required="">
                                        
                                            <option value="">Select One</option>
                                            <option value="1" <?php if($rec->relationid==1){ echo "selected";}?>>Father</option>
                                            <option value="2" <?php if($rec->relationid==2){ echo "selected";}?>>Mother</option>
                                            <option value="3" <?php if($rec->relationid==3){ echo "selected";}?>>Brother</option>
                                            <option value="4" <?php if($rec->relationid==4){ echo "selected";}?>>Sister</option>
                                            <option value="5" <?php if($rec->relationid==5){ echo "selected";}?>>Wife</option>
                                            <option value="6" <?php if($rec->relationid==6){ echo "selected";}?>>Daughter</option>
                                            <option value="7" <?php if($rec->relationid==7){ echo "selected";}?>>Son</option>
                                        
                                        </select>
                                    </div>
                                </div>
                                
                                <div class='form-group'>                              
                                    <label class="col-sm-2 control-label" for="age" >Age</label>
                                    <div class="col-sm-4">
                                        <select name="age" class="form-control" id="relation" required="">
                                        
                                            <option value="">Select One</option>
                                            <?php for($i=1;$i<=100;$i++):?>
                                                <option value="<?php echo $i;?>" <?php if($rec->age==$i){ echo "selected";}?>><?php echo $i;?></option>
                                            <?php    endfor;?>
                                        
                                        </select>
                                    </div>
                                
                                    
                                    <label class="col-sm-2 control-label" for="status">Status</label>
                                    <div class="col-sm-4">
                                        <select name="status" class="form-control" id="status" required="">
                                        <?php //print_r($records);exit;?>
                                            <option value="">Select One</option>
                                            <option value="1" <?php if($rec->isalive){?> selected="selected"  <?php }else{}?> >Alive</option>
                                            <option value="0" <?php if(!$rec->isalive){?> selected="selected"  <?php }else{}?> >Dead</option>
                                                                                    
                                        </select>
                                    </div>
                                </div>  
                                        
                                
                                
                                <!--start of address and City Fields-->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="address">Address</label>
                                    <div class="col-sm-4">
                                        <input name="address" class="form-control" id="address" required="" value="<?php echo $rec->permaddress;?>" type="text" placeholder="required"/>
                                        </br>
                                     <!--   <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="staff_address">As of Staff
                                            </label>
                                        </div>-->
                                    </div>
                                    <?php //print_r($rec);exit;?>
                                    <label class="col-sm-2 control-label" for="city">City</label>
                                    <div class='col-sm-4'>    
                                    <?php //print_r($cities);exit;?>
                                        <select name="city" class="form-control" required="">
                                            <option value="">Select City</option>
                                            <?php foreach($cities as $city){?>
                                            <?php if($rec->city==$city->Name){?>
                                             <option selected="selected" value="<?php echo $city->Name;?>" ><?php echo $city->Name;?></option>
                                            <?php }else{ ?>
                                            <option value="<?php echo $city->Name;?>"><?php echo $city->Name;?></option>
                                            
                                         <?php }}?>
                                            
                                         

                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="profession">Profession</label>
                                    <div class="col-sm-4">
                                        <input name="profession" class="form-control" id="profession" required="" value="<?php echo $rec->profession;?>" type="text" placeholder="required"/>
                                        </br>
                                        <div class="checkbox" name="checknok">
                                            <label>
                                                <input type="checkbox" name="checknok" value="yes" <?php if($rec->isnok=="yes"){ echo "checked";}?>> NOK
                                            </label>
                                        </div>
                                    </div>
                                    <label class="col-sm-2 control-label" for="distr">Distr</label>
                                    <div class='col-sm-4'>    
                                        <input name="distr" class="form-control" required="" type="text" value="<?php echo $rec->distr;?>" placeholder="This is Required"/>
                                    </div>
                                </div>
                                
                                <!-- End of address field -->
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Cell No</label>
                                    <div class="col-md-2 col-sm-3">
                                        <select name="phone_code" class="form-control" >
                                            <option value="">Select Code</option>
                                            <option value="0300">0300</option>
                                            <option value="0301">0301</option>
                                            <option value="0302">0302</option>
                                            <option value="0303">0303</option>
                                            <option value="0304">0304</option>
                                            <option value="0305">0305</option>
                                            <option value="0306">0306</option>
                                            <option value="0307">0307</option>
                                            <option value="0308">0308</option>
                                            <option value="0309">0309</option>
                                            <option value="0310">0310</option>
                                            <option value="0311">0311</option>
                                            <option value="0312">0312</option>
                                            <option value="0313">0313</option>
                                            <option value="0314">0314</option>
                                            <option value="0315">0315</option>
                                            <option value="0320">0320</option>
                                            <option value="0321">0321</option>
                                            <option value="0322">0322</option>
                                            <option value="0323">0323</option>
                                            <option value="0324">0324</option>
                                            <option value="0325">0325</option>
                                            <option value="0331">0331</option>
                                            <option value="0332">0332</option>
                                            <option value="0333">0333</option>
                                            <option value="0334">0334</option>
                                            <option value="0335">0335</option>
                                            <option value="0336">0336</option>
                                            <option value="0340">0340</option>
                                            <option value="0341">0341</option>
                                            <option value="0342">0342</option>
                                            <option value="0343">0343</option>
                                            <option value="0344">0344</option>
                                            <option value="0345">0345</option>
                                            <option value="0346">0346</option>
                                            <option value="0347">0347</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input  name="p_number" class="form-control"  id="p_number" type="text"  placeholder="It is Required">
                                    </div>
                                </div>
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <input class="btn btn-default" type="submit" value="update Dependent"/>
                                        
                                         <input class="btn btn-default" type="reset" value="Cancel"/>
                                    </div>
                                    
                                </div>
								<?php }}else{ echo "no data check again";}?>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
       </div>
        <!-- /.col-lg-12 -->
        <!-- End Validation States -->
    </div>
</div>
