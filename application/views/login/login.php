
<div class="login">
    
    <div class="container">
        <div class="row">

            <div class="col-md-4 col-md-offset-4">
                <?php echo form_open(base_url()."user_cont/validate_credentials");?>

                <div class="login-banner text-center">

                    <h1>

                        <i class="fa fa-gears"></i> Login In

                    </h1>

                </div>
                
                <?php
                        if(isset($error_login)):?>
                <div class="tile red">
                    
                    <h3> Error Logging In, Check Username And Password Again</h3>
                    
                    
                </div>
                <?php   endif;?>
                <div class="portlet portlet-green">
                    <div class="portlet-heading login-heading">
                        <div class="portlet-title">
                            <h4><strong>Login!</strong>
                            </h4>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    
                    <div class="portlet-body">
                        
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="email" type="email">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>
                                <br>
                                <input class="btn btn-lg btn-green btn-block" type="submit" Value="Sign In" />
                            </fieldset>
                            <br>
                            <p class="small">
                                <a href="">Forgot your password?</a>
                            </p>
                        <?php echo form_close();?>
                        <?php echo validation_errors();?>
                    </div>


                </div>

            </div>

        </div>


    </div>
</div>