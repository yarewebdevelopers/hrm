
<div class="login">
    
    <div class="container">
        <div class="row">

            <div class="col-md-4 col-md-offset-4">

                <?php echo form_open(base_url()."user_controller/check_Login");?>

                <div class="login-banner text-center">

                    <h1>

                        <i class="fa fa-gears"></i> Login 

                    </h1>

                </div>
                
               
                <div class="portlet portlet-green">
                    <div class="portlet-heading login-heading">
                        <div class="portlet-title">
                            <h4><strong>Login!</strong>
                            </h4>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    
                    <div class="portlet-body">
                        
                            <fieldset>
                                <div class="form-group">
                                    <input class="form-control" placeholder="E-mail" name="email" type="email">
                                </div>
                                <div class="form-group">
                                    <input class="form-control" placeholder="Password" name="password" type="password" value="">
                                </div>
                                <div class="checkbox">
                                    <label>
                                        <input name="remember" type="checkbox" value="Remember Me">Remember Me
                                    </label>
                                </div>
                                <br>
                                <input class="btn btn-lg btn-green btn-block" type="submit" Value="Sign In" />
                            </fieldset>
                            <br>
							 <?php
                        if(isset($error_message)):?>
								
                    
									<p style="color:red">Check Username And Password Again<p>
                    
                    
									
							<?php   endif;?>
                            <p class="small">
                                <a href="">Forgot your password?</a>
                            </p>
                        <?php
                    if(isset($error_message)):?>
                        <div class="alert alert-danger alert-dismissable">
                             <button class="close" aria-hidden="true" type="button" data-dismiss="alert">×</button>
                             <strong>Error Logging In</strong><br /> Check Username And Password Again
                         </div>
                
                <?php   endif;?>    
                        <?php echo form_close();?>

                        <?php if( validation_errors()):?>
                            <div class="alert alert-danger alert-dismissable">
                             <button class="close" aria-hidden="true" type="button" data-dismiss="alert">×</button>
                             <strong>Error Logging In</strong><br /> <?php echo validation_errors();?>
                         </div>
                            <?php endif; ?>
                    </div>


                </div>

            </div>

        </div>


    </div>
</div>