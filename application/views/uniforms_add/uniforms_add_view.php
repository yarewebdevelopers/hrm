<div id="page-wrapper">


    <div class="page-content">

        <!-- begin PAGE TITLE ROW -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>Staff Management
                        
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i>  <a href="index.html">Staff Management</a>
                        </li>
                        <li class="active">Add Staff Uniform</li>
                    </ol>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>


       <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Add Staff Uniform</h4>
                        </div>
                        <div class="clearfix">
                            
                        </div>
                    </div>
                    <div class="panel-collapse collapse in" id="validationExamples">
                        <div class="portlet-body">
                            <form class="form-horizontal" id="validate" role="form" novalidate="novalidate">
                                
                                <!-- Start of father's name name and number fields -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="number">Number</label>
                                    <div class="col-sm-4">
                                        <input name="number" class="form-control" id="number" disabled="" type="text"/>
                                    </div>
                                
                                    <label class="col-sm-2 control-label" for="name">Name</label>
                                    <div class="col-sm-4">
                                        <input name="name" class="form-control" id="name" disabled="" type="text"/>
                                    </div>
                                </div>
                                
                                <div class='form-group'>                              
                                
                                    <label class="col-sm-2 control-label" for="rank">Rank</label>
                                    <div class="col-sm-4">
                                        <input name="rank" class="form-control" id="rank" disabled="" type="text"/>
                                    </div>
                                </div>
                                
                                <div class="form-group">    
                                    <label class="col-sm-2 control-label" for="award">Uniform Number</label>
                                    <div class="col-sm-4">
                                        <input name="award_code" class="form-control" id="award_code" required="" placeholder="It is Required"/>
                                    </div>
                                 
                                                                    
                                <!--Start of DOB, DOJ, DOR fields-->
                                
                                    <label class="col-sm-2 control-label" for="dob">BD Processing Date</label>
                                    <div class="col-sm-4">
                                        <input type="award_name" name="attach_from" id="award_name" class="form-control" required="" placeholder="It is Required"/>
                                    </div>
                                </div> 
                                <div class="form-group">    
                                    <label class="col-sm-2 control-label" for="award">Remarks</label>
                                    <div class="col-sm-4">
                                        <textarea name="award_code" rows="3" class="form-control" id="award_code" required="" placeholder="It is Required"/>
                                    </textarea>
                                    </div>
                                 
                                                                    
                                <!--Start of DOB, DOJ, DOR fields-->
                                
                                    
                                </div> 
                                
                           
                                
                                                                                               
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <input class="btn btn-default" type="submit" value="Add Uniform"/>
                                        
                                         <input class="btn btn-default" type="reset" value="Cancel"/>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
       </div>
        <!-- /.col-lg-12 -->
        <!-- End Validation States -->
    </div>
</div>
