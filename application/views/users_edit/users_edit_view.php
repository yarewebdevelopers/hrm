<div id="page-wrapper">


    <div class="page-content">

        <!-- begin PAGE TITLE ROW -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>User Management
                        
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i>  <a href="index.html">User Management</a>
                        </li>
                        <li class="active">Edit User</li>
                    </ol>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>


       <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Edit User Form</h4>
                        </div>
                        <div class="clearfix">
                            
                        </div>
                    </div>
                    <div class="panel-collapse collapse in" id="validationExamples">
                        <div class="portlet-body">
                            <form class="form-horizontal" id="validate" role="form" novalidate="novalidate" method="post" action="<?php echo base_url('index.php/user_controller/update_user/'.$idno);?>">
                                
                                <!-- Start of father's name name and number fields -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="number">Email</label>
                                    <div class="col-sm-4">
                                        <input name="email" readonly class="form-control" id="number" value="<?php echo $edit_data[0]['email'];?>" required type="email" />
                                    </div>
                                
                                    <label class="col-sm-2 control-label" for="name">Name</label>
                                    <div class="col-sm-4">
                                        <input name="name" class="form-control" id="name" required=""  type="text" placeholder="<?php echo $edit_data[0]['name'];?>" pattern="[A-Za-z]+"/>
                                    </div>
                                </div>
                                
                                <div class='form-group'>                              
                                    <label class="col-sm-2 control-label" for="father" >Password</label>
                                    <div class="col-sm-4">
                                        <input name="password" class="form-control" id="password"  type="password" required placeholder="<?php echo $edit_data[0]['password'];?>"/>
                                    </div>
                                
                                    
                                  <!--  <label class="col-sm-2 control-label" for="cnic">Confirm Password</label>
                                    <div class="col-sm-4">
                                        <input name="c_password" class="form-control" id="c_password" required="" type="password" value="" placeholder="It is Required"/>
                                    </div>-->
                                </div>  
                                        
                                <div class="form-group">     
                                    <label class="col-sm-2 control-label">Role</label>
                                    <div class="col-sm-4">
                                        <select name="role" class="form-control" required>
                                            <option value="">Select One:</option>
                                            <option value="2">Admin</option>
                                            <option value="3">employee</option>
                                            
                                        </select>
                                    </div>
                                
                                    
                                    
                                    <label class="col-sm-2 control-label" for="reporting">Department</label>
                                    <div class="col-sm-4">
                                        <select name="office" class="form-control" required>
                                            <option value="">Select One:</option>
                                            
                                            
                                            <option value="1">P bty</option>
                                            <option value="2">Q bty</option>
                                            <option value="3">R bty</option>
											<option value="5">HQ bty</option>
                                        </select>
                                            
                                    </div>
                                </div>
                                <!-- end of CNIC, rank and reporting to-->
                                    
                                
                                                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <input class="btn btn-default" type="submit" value="Update User"/>
                                        
                                         <input class="btn btn-default" type="reset" value="Cancel"/>
                                    </div>
                                    
                                </div>
								<div>
								<?php echo validation_errors();?>
								</div>
								
                            </form>
                        </div>
                    </div>
                </div>
            </div>
       </div>
        <!-- /.col-lg-12 -->
        <!-- End Validation States -->
    </div>
</div>
