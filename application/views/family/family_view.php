<div id="page-wrapper">
<?php //print_r($cities);exit;?>
    <div class="page-content page-content-ease-in">

        <!-- begin PAGE TITLE ROW -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>Family Management
					
                        <a href="<?php echo base_url('emp_family_cont/load_add_emp_family_page/'.$person_record[0]->id);?>"><input type="submit" class="btn btn-default" value="Add Dependent"/></a>
						<a href="<?php echo base_url('emp_family_cont/load_deleted_family_page/'.$person_record[0]->id)?>"><input class="btn btn-danger" type="submit" value="Trash"/></a>
				   </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url('emp_emp_cont');?>">Staff Management</a>
                        </li>
                        <li class="active">Family</li>
                    </ol>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <!-- end PAGE TITLE ROW -->
        <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Employee Profile</h4>
                        </div>
                        <div class="clearfix">
                            
                        </div>
                    </div>
                    <div class="panel-collapse collapse in" id="validationExamples">
                        <div class="portlet-body">
						<?php if(isset($person_record)){
                                
                               foreach($person_record as $pr){?>
                                <div class="form-group">
								
								
								
                                    <label class="col-sm-2 control-label" for="number">Number</label>
                                    <div class="col-sm-4">
                                        <span><?php echo $pr->no;?></span>
                                        
                                    </div>
									
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="name">Rank/Name</label>
                                    <div class="col-sm-4">
                                        <span><?php echo $pr->rank;?>&nbsp;&nbsp;<?php echo $pr->name;?></span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="name">Office</label>
                                    <div class="col-sm-4">
                                        <span><?php echo $pr->depname;?></span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
							<?php	}}else{
								echo "no data";
								}?>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- begin ADVANCED TABLES ROW -->
        <div class="row">

            <div class="col-lg-12">

                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Family</h4>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-responsive">
						
						
                            <div role="grid" class="dataTables_wrapper form-inline" id="example-table_wrapper">

                                <table class="table table-striped table-bordered table-hover table-green dataTable" id="example-table" aria-describedby="example-table_info">

                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width:auto;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                Sr.#
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Browser: activate to sort column ascending">
                                                Name
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Platform(s): activate to sort column ascending">
                                                Age
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Engine version: activate to sort column ascending">
                                                Relation
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Engine version: activate to sort column ascending">
                                                Status
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Engine version: activate to sort column ascending">
                                                Profession
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Engine version: activate to sort column ascending">
                                                Address
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Engine version: activate to sort column ascending">
                                                Contact No.
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Engine version: activate to sort column ascending">
                                                NOK
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Engine version: activate to sort column ascending">
                                                Distr
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="CSS grade: activate to sort column ascending">
                                                Actions
                                            </th>
                                        </tr>
                                    </thead>
									

                                    <tbody role="alert" aria-live="polite" aria-relevant="all">
									<?php if($records!=false){
									$count=1;
									foreach($records as $rec){
									
									?>
                                        <tr class="gradeA odd">
                                            <td class="  sorting_1"><?php echo $count;?></td>
                                            <td class=" "><?php echo $rec->famname;?></td>
                                            <td class=" "><?php echo $rec->age;?></td>
                                            <td class=" "><?php echo $rec->relation;?></td>
                                            <td class="center "><?php echo $rec->status;?></td>
                                            <td class="center "><?php echo $rec->profession;?></td>
                                            <td class="center "><?php echo $rec->permaddress;?></td>
                                            <td class="center "><?php echo $rec->contactno;?></td>
                                            <td class="center "><?php echo $rec->isnok;?></td>
                                            <td class="center "><?php echo $rec->distr;?></td>
                                            <td class="center ">
                                                <div class="tooltip-demo">
                                                    <a href="<?php echo base_url('emp_family_cont/load_update_emp_family_record_page/'.$rec->id);?>"><li class="fa fa-edit" data-original-title="Edit Dependent Profile" data-placement="bottom" data-toggle="tooltip"></li></a>
                                                   <a href="<?php echo base_url('emp_family_cont/delete_emp_family_record/'.$rec->id);?>"> <li class="fa fa-trash-o" data-original-title="Delete Dependent" data-placement="bottom" data-toggle="tooltip"></li>
                                                </div>
                                            </td>
                                        </tr>
                                        
                                     <?php $count++;}}
									 else{
									 
									 }
									 ?>   
                                      
                                </tbody>
                            </table>

                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.portlet-body -->
                </div>
                <!-- /.portlet -->

            </div>
            <!-- /.col-lg-12 -->

        </div>
        <!-- /.row -->

    </div>
    <!-- /.page-content -->

    </div>
</div>