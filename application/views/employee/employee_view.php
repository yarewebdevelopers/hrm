<div id="page-wrapper">

<div class="page-content page-content-ease-in">

    <!-- begin PAGE TITLE ROW -->
    <div class="row">
        <div class="col-lg-12">
            <div class="page-title">
                <h1>Staff Management
                    <a href="<?php echo base_url('emp_emp_cont/addEmp');?>"><input class="btn btn-default" type="submit" value="Add Staff"/></a>
                    <?php if(!isset($trash)){?>
                    <a href="<?php echo base_url('emp_emp_cont/load_deleted_emp/')?>"><input class="btn btn-danger" type="submit" value="Trash"/></a>
                <?php }else{?>
                <a href="<?php echo base_url('emp_emp_cont/')?>"><input class="btn btn-default" type="submit" value="Back"/></a>
                <?php }?>

                </h1>
                <ol class="breadcrumb">
                    <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url('welcome/');?>">Dashboard</a>
                    </li>
                    <li class="active">Staff Directory</li>
                </ol>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- end PAGE TITLE ROW -->
                        <div class="error">
                    <?php if(isset($msg)){?>
                    	
                    	
                    	
                    	
                    	
                    
                        
                     <div class="alert alert-success" role="alert">
                                        <span class="fa fa-thumbs-up" aria-hidden="true"></span>
                                        <span class="sr-only">Message:</span>
                                        <?php echo $msg;
 
                                        
                                        ?>
                                                                   
                                       
                                        
                                    </div>
                    <?php }?>
                    
                    </div>

    <!-- begin ADVANCED TABLES ROW -->
    <div class="row">

        <div class="col-lg-12">

            <div class="portlet portlet-default">
                <div class="portlet-heading">
                    <div class="portlet-title">
                        <h4>Staff Directory</h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <div role="grid" class="dataTables_wrapper form-inline" id="example-table_wrapper">

                            <table class="table table-striped table-bordered table-hover table-green dataTable" id="example-table" aria-describedby="example-table_info">

                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width:auto;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                            Sr.#
                                        </th>
                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Browser: activate to sort column ascending">
                                            Army No.
                                        </th>

                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Engine version: activate to sort column ascending">
                                            Rank/Name
                                        </th>

                                     <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="CSS grade: activate to sort column ascending">
                                            CNIC
                                        </th>
                                          <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="CSS grade: activate to sort column ascending">
                                            DOB
                                        </th>
                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="CSS grade: activate to sort column ascending">
                                            Join Date
                                        </th>
                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="CSS grade: activate to sort column ascending">
                                            Total Service
                                        </th>
                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="CSS grade: activate to sort column ascending">
                                            Cell No.
                                        </th>
                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="CSS grade: activate to sort column ascending">
                                            Caste
                                        </th>
                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="CSS grade: activate to sort column ascending">
                                            Blood Group
                                        </th>
                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="CSS grade: activate to sort column ascending">
                                            Actions
                                        </th>
                                    </tr>
                                </thead>

                                <tbody role="alert" aria-live="polite" aria-relevant="all">
                                <?php if(isset($records)){ ?>
                                <?php foreach($records as $record){?>
                                    <tr class="gradeA odd">
                                        <td class="  sorting_1"><?php echo $record->id;?></td>
                                        <td class=" "><?php echo $record->no;?></td>
                                        <td class=" "><?php echo $record->rank."  ".$record->name;?></td>
                                        <td class="center "><?php echo $record->cnicno; ?></td>
                                        <td class="center "><?php echo $record->dob; ?></td>
                                        <td class="center "><?php echo $record->doj; ?></td>
                                        <td class="center "><?php $datetime1 = new DateTime();
                                         $datetime2 = new DateTime($record->doj);
                                         $interval = $datetime1->diff($datetime2);
                                         echo $interval->format('%y - %m - %d '); ?></td>
                                        <td class="center "><?php echo $record->cellno; ?></td>
                                        <td class="center "><?php echo $record->cast; ?></td>
                                         <td class="center "><?php echo $record->bloodgroup; ?></td>
                                         
                                          <?php if(!isset($trash)){?>
                                        <td>
                                        
                                            
                                                <a class="tooltip-demo" href="<?php echo base_url();?>emp_emp_cont/getEmpById/<?php echo $record->id;?>">
                                                    <li class="fa fa-search" title="View Staff Profile" data-placement="bottom" data-toggle="tooltip">
                                                        
                                                    </li>
                                                </a>
                                                <a class="tooltip-demo" href="<?php echo base_url();?>emp_family_cont/get_emp_family_record/<?php echo $record->id;?>" title="">
                                                    <li class="fa fa-home" title="View Staff Family" data-placement="bottom" data-toggle="tooltip">
                                                        
                                                    </li>
                                                </a>
                                                <a class="tooltip-demo" href="<?php echo base_url();?>emp_emp_cont/updateEmp/<?php echo $record->id;?>" title="" >
                                                    <li class="fa fa-edit" title="Edit Staff Profile" data-placement="bottom" data-toggle="tooltip" >
                                                        
                                                    </li>
                                                </a>
                                                <a class="tooltip-demo" href="<?php echo base_url();?>emp_emp_cont/deleteEmp/<?php echo $record->id;?>" title="" >
                                                    <li class="fa fa-trash-o" title="Delete Staff Profile" data-placement="bottom" data-toggle="tooltip"></li>
                                                </a>
                                                <a class="tooltip-demo" href="<?php echo base_url();?>emp_att_cont/getAttById/<?php echo $record->id;?>" >
                                                    <li class="fa fa-paperclip" title="Create Staff Attachments" data-placement="bottom" data-toggle="tooltip">
                                                        
                                                    </li>
                                                </a>
                                                <a class="tooltip-demo" href="<?php echo base_url();?>emp_csd_cont/getCSDById/<?php echo $record->id;?>">
                                                    <li class="fa fa-shopping-cart" title="View Staff Procurement" data-placement="bottom" data-toggle="tooltip">
                                                        
                                                    </li>
                                                </a>



                                                <a class="tooltip-demo" href="<?php echo base_url();?>emp_leave_cont/getLeaveRecordById/<?php echo $record->id;?>" >

                                                    <li class="fa fa-wheelchair" data-original-title="View Staff Leaves" data-placement="bottom" data-toggle="tooltip">
                                                        
                                                    </li>
                                                </a>
                                                <a class="tooltip-demo" href="<?php echo base_url();?>emp_award_cont/getEmpAwardById/<?php echo $record->id;?>" >
                                                    <li class="fa fa-trophy" title="View Staff Awards" data-placement="bottom" data-toggle="tooltip">
                                                        
                                                    </li>
                                                </a>
                                                <a class="tooltip-demo" href="<?php echo base_url();?>emp_acr_cont/getacrbyid/<?php echo $record->id;?>" >
                                                    <li class="fa fa-file-text" title="View ACR" data-placement="bottom" data-toggle="tooltip">
                                                        
                                                    </li>
                                                </a>
                                                <a class="tooltip-demo"href="<?php echo base_url();?>emp_uniform_cont/geturecordbyid/<?php echo $record->id;?>" >
                                                    <li class="fa fa-male" title="View Uniforms" data-placement="bottom" data-toggle="tooltip">
                                                        
                                                    </li>
                                                </a>
                                                <a class="tooltip-demo" href="<?php echo base_url();?>emp_medical_cont/getEmpMedicalById/<?php echo $record->id;?>" >
                                                    <li class="fa fa-medkit" title="View Medical Record" data-placement="bottom" data-toggle="tooltip">
                                                        
                                                    </li>
                                                </a>
                                                <a class="tooltip-demo" href="<?php echo base_url();?>emp_ach_cont/getachbyid/<?php echo $record->id;?>" >
                                                    <li class="fa fa-star" title="View Achievements" data-placement="bottom" data-toggle="tooltip">
                                                        
                                                    </li>
                                                </a>
                                                <a class="tooltip-demo" href="<?php echo base_url();?>emp_lacking_cont/getLclassbyid/<?php echo $record->id;?>" >
                                                    <li class="fa fa-pencil" title="View Classes" data-placement="bottom" data-toggle="tooltip">
                                                        
                                                    </li>
                                                </a>
                                            
                                        </td>
                                        <?php }else{?>
                                        <td>
                                                     <a class="tooltip-demo" href="<?php echo base_url();?>emp_emp_cont/recover/<?php echo $record->id;?>" >
                                                    <li class="fa fa-undo" title="Recover" data-placement="bottom" data-toggle="tooltip">
                                                        
                                                    </li>
                                                </a>
                                        
                                        </td>
                                        <?php }?>
                                    </tr>
                                    <?php }}?>
                                                             </tbody>
                        </table>

                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.portlet-body -->
            </div>
            <!-- /.portlet -->

        </div>
        <!-- /.col-lg-12 -->

    </div>
    <!-- /.row -->

</div>
<!-- /.page-content -->

</div>
</div>