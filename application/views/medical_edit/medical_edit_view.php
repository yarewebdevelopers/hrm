<div id="page-wrapper">


    <div class="page-content">

        <!-- begin PAGE TITLE ROW -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>Staff Management
                        
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url('emp_emp_cont');?>">Staff Management</a>
                        </li>
                        <li class="active">Edit Medical Record</li>
                    </ol>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>


       <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Edit Medical Record Form</h4>
                        </div>
                        <div class="clearfix">
                            
                        </div>
                    </div>
                    <div class="panel-collapse collapse in" id="validationExamples">
                        <div class="portlet-body">
                            <form class="form-horizontal" id="validate" role="form" novalidate="novalidate" method="POST" action="<?php echo base_url('emp_medical_cont/updateMedical/'.$records[0]->id.'/'.$meds[0]->id);?>">
                                
                                <!-- Start of father's name name and number fields -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="number">Number</label>
                                    <div class="col-sm-4">
                                        <input name="number" class="form-control" id="number" disabled type="text" placeholder="It is Required" value="<?php echo $records[0]->no;?>"/>
                                    </div>
                                
                                    <label class="col-sm-2 control-label" for="name">Name</label>
                                    <div class="col-sm-4">
                                        <input name="name" class="form-control" id="name" required="" type="text" placeholder="It is Required" value="<?php echo $records[0]->name;?>"/>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="address">Disease</label>
                                    <div class="col-sm-4">
                                        <input name="diseases" class="form-control" id="diseases" required="" type="text" placeholder="" value="<?php echo $meds[0]->diseases;?>"/>
                                    </div>
                                    <label class="col-sm-2 control-label" for="address">Disposal</label>
                                    <div class='col-sm-4'>    
                                        <input name="disposal" class="form-control" id="disposal" required="" type="text" placeholder="It is Required" value="<?php echo $meds[0]->disposal;?>"/>
                                    </div>
                                </div> 
                                        
                                <div class="form-group">     
                                    <label class="col-sm-2 control-label">Category</label>
                                    <div class="col-sm-4">
                                        <select name="cat" class="form-control" required="">
                                            <option value="">Select One:</option>
                                            <option <?php if($meds[0]->cat=="Category A"):?>selected="" <?php endif;?> value="Category A">Category A</option>
                                            <option <?php if($meds[0]->cat=="Category B"):?>selected="" <?php endif;?> value="Category B">Category B</option>
                                            <option <?php if($meds[0]->cat=="Category C"):?>selected="" <?php endif;?> value="Category C">Category C</option>
                                            
                                        </select>
                                    </div>
                                
                                    
                                    
                                    <label class="col-sm-2 control-label" for="reporting">Admitted CMH</label>
                                    <div class="col-sm-4">
                                        <select name="cmh" class="form-control" required="">
                                            
                                            <option value="">Select One:</option>
                                            <option <?php if($meds[0]->admittedcmh=="Yes"):?>selected="" <?php endif;?>value="Yes">Yes</option>
                                            <option <?php if($meds[0]->admittedcmh=="No"):?>selected="" <?php endif;?>value="No">No</option>
                                            
                                            </select>
                                    </div>
                                </div>
                                <!-- end of CNIC, rank and reporting to-->
                                    
                                <!--Start of DOB, DOJ, DOR fields-->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="dob">Admitted From</label>
                                    <div id="sandbox-container" class="col-sm-4">
                                        <input type="text" name="from" id="from" class="form-control" required="" placeholder="It is Required" value="<?php echo $meds[0]->from;?>"/>
                                    </div>
                                
                                
                                    <label class="col-sm-2 control-label" for="doj">Admitted To</label>
                                    <div class="col-sm-4" id="sandbox-container">
                                        <input name="to" class="form-control" id="to" required="" type="text" placeholder="It is Required" value="<?php echo $meds[0]->to;?>"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                
                                    <label class="col-sm-2 control-label" for="doj" >Review Date</label>
                                    <div class="col-sm-10 col-md-4" id="sandbox-container">
                                        <input name="review" class="form-control" id="review" required="" type="text" placeholder="It is Required" value="<?php echo $meds[0]->review;?>"/>
                                    </div>
                                    <label class="col-sm-2 control-label" for="doj" >Partiio</label>
                                    <div class="col-sm-10 col-md-4" id="sandbox-container">
                                        <input name="partiio" class="form-control" id="partiio" required="" type="text" value="<?php echo $meds[0]->partiio;?>" placeholder="It is Required" value="<?php echo $meds[0]->review;?>"/>
                                    </div>
                                
                                </div>
                                <!--end of DOB, DOJ, DOR fields-->
                                
                                <!--start of address and City Fields-->
                                
                                
                                <!-- End of address field -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Remarks</label>
                                    <div class="col-sm-4">
                                        <textarea  rows="3" name="remarks" class="form-control" id="remarks" required="" type="text" placeholder="It is Required"><?php echo $meds[0]->remarks;?></textarea>
                                    </div>
                                
                                    
                                </div>
                                             
                                                             
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <input class="btn btn-default" type="submit" name="submit" value="Edit Medical History"/>
                                        
                                         <input class="btn btn-default" type="reset" value="Cancel"/>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
       </div>
        <!-- /.col-lg-12 -->
        <!-- End Validation States -->
    </div>
</div>
