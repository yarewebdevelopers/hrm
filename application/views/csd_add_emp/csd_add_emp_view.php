<div id="page-wrapper">


    <div class="page-content">

        <!-- begin PAGE TITLE ROW -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>Staff Management
                        
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i>  <a href="index.html">Staff Management</a>
                        </li>
                        <li class="active">Add CSD Procurement</li>
                    </ol>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>


       <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Add CSD Procurement Form</h4>
                        </div>
                        <div class="clearfix">
                            
                        </div>
                    </div>
                    <div class="panel-collapse collapse in" id="validationExamples">
                        <div class="portlet-body">
                            <form class="form-horizontal" id="validate" role="form" novalidate="novalidate" method="post" action="<?php echo base_url('emp_csd_cont/AddCsd/'.$this->uri->segment(3));?>">
                                
                                <!-- Start of father's name name and number fields -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="number">Number</label>
                                    <div class="col-sm-4">
                                        <input name="number" class="form-control" id="number" disabled="" value="<?php echo $records[0]->no;?>" type="text"/>
                                    </div>
                                
                                    <label class="col-sm-2 control-label" for="name">Name</label>
                                    <div class="col-sm-4">
                                        <input name="name" class="form-control" id="name" disabled="" value="<?php echo $records[0]->name;?>" type="text"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="rank">Rank</label>
                                    <div class="col-sm-4">
                                        <input name="rank" class="form-control" id="rank" disabled="" value="<?php echo $records[0]->rank;?>" type="text"/>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="item">Item Name</label>
                                    <div class="col-sm-4">
                                        <input name="items" class="form-control" id="items" required="" type="text"/>
                                    </div>
                  
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="installmentpaid">Installments Paid</label>
                                    <div class="col-sm-4">
                                        <input name="installmentspaid" class="form-control" id="installmentspaid" required="" type="number"/>
                                    </div>
                                    <label class="col-sm-2 control-label" for="name">Installment Date</label>
                                    <div class="col-sm-4" id="sandbox-container">
                                        <input name="installmentdate" class="form-control" id="installmentdate" required="" type="text"/>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="installmentpending">Installments Pending</label>
                                    <div class="col-sm-4">
                                        <input name="installmentspending" class="form-control" id="installmentspending" required="" type="number" min="1" />
                                    </div>
                                    <label class="col-sm-2 control-label" for="name">Installments Delayed</label>
                                    <div class="col-sm-4">
                                        <input name="installmentsdelayed" class="form-control" id="installmentsdelayed" required="" type="number"/>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="remarks">Remarks</label>
                                    <div class="col-sm-4">
                                        <textarea name="remarks" class="form-control" id="remarks" type="text" rows="3"></textarea>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <input class="btn btn-default" type="submit" name="submit" value="Add Pocurement"/>
                                        
                                         <input class="btn btn-default" type="reset" value="Cancel"/>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
       </div>
        <!-- /.col-lg-12 -->
        <!-- End Validation States -->
    </div>
</div>
