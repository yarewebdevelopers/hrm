<div id="page-wrapper">

<?php // print_r($cities);exit;?>
    <div class="page-content">

        <!-- begin PAGE TITLE ROW -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>Staff Family Management
					<a href="<?php echo base_url('emp_family_cont/get_emp_family_record/'.$person_record[0]->id);?>"><input type="submit" class="btn btn-default" value="back"/></a>
                        
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url('emp_family_cont/get_emp_family_record/'.$person_record[0]->id);?>">Staff Management</a>
                        </li>
                        <li class="active">Add Staff Family</li>
                    </ol>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
		


       <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Add Staff Family Form</h4>
                        </div>
                        <div class="clearfix">
                            
                        </div>
                    </div>
					
                    <div class="panel-collapse collapse in" id="validationExamples">
                        <div class="portlet-body">
                            <form class="form-horizontal" id="validate" role="form" novalidate="novalidate" method="post" action="<?php echo base_url('emp_family_cont/add_emp_family_record/'.$person_record[0]->id);?>">
                                
                                <!-- Start of father's name name and number fields -->
                                <div class="form-group">
                                                                  
                                    <label class="col-sm-2 control-label" for="name">Name</label>
                                    <div class="col-sm-4">
                                        <input name="name" class="form-control" id="name" required=""  type="text" placeholder="It is Required"/>
                                    </div>
                                    <label class="col-sm-2 control-label" for="name">Relation</label>
                                    <div class="col-sm-4">
                                        <select name="relation" class="form-control" id="relation" required="">
                                        
                                            <option value="">Select One</option>
                                            <option value="1">Father</option>
                                            <option value="2">Mother</option>
                                            <option value="3">Brother</option>
                                            <option value="4">Sister</option>
                                            <option value="5">Wife</option>
                                            <option value="6">Daughter</option>
                                            <option value="7">Son</option>
                                        
                                        </select>
                                    </div>
                                </div>
                                
                                <div class='form-group'>                              
                                    <label class="col-sm-2 control-label" for="father" >Age</label>
                                    <div class="col-sm-4">
                                        <select name="age" class="form-control" id="age" required="">
                                        
                                            <option value="">Select One</option>
                                            <?php for($i=1;$i<=100;$i++):?>
                                                <option value="<?php echo $i;?>"><?php echo $i;?></option>
                                            <?php    endfor;?>
                                        
                                        </select>
                                    </div>
                                
                                    
                                    <label class="col-sm-2 control-label" for="status">Status</label>
                                    <div class="col-sm-4">
                                        <select name="status" class="form-control" id="status" required="">
                                        
                                            <option value="">Select One</option>
                                            <option value="alive">Alive</option>
                                            <option value="dead">Dead</option>
                                                                                    
                                        </select>
                                    </div>
                                </div>  
                                        
                                
                                
                                <!--start of address and City Fields-->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="address">Address</label>
                                    <div class="col-sm-4">
                                        <input name="address" class="form-control" id="address1" required=""  type="text" placeholder="insert address"/>
                                        </br>
                                       <!-- <div class="checkbox">
                                            <label>
                                                <input type="checkbox" value="staff_address">As of Staff
                                            </label>
                                        </div>-->
                                    </div>
                                    <label class="col-sm-2 control-label" for="city">City</label>
                                    <div class='col-sm-4'>    
                                      <?php //print_r($cities);exit;?>
                                        <select name="city" class="form-control" required="">
                                            <option value="" >Select City</option>
                                           <?php foreach($cities as $city){?>
                                         
                                            <option value="<?php echo $city->Name;?>"><?php echo $city->Name;?></option>
                                           <?php }?>
                                            
                                           
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="profession">Profession</label>
                                    <div class="col-sm-4">
                                        <input name="profession" class="form-control" id="profession" required=""  type="text" placeholder="insert profession"/>
                                        </br>
                                        <div class="checkbox">
                                            <label>
                                                <input type="checkbox"  name="checknok" value="yes"> NOK
                                            </label>
                                        </div>
                                    </div>
                                    <label class="col-sm-2 control-label" for="distr">Distr</label>
                                    <div class='col-sm-4'>    
                                        <input name="distr" class="form-control" required="" type="text" placeholder="This is Required"/>
                                    </div>
                                </div>
                                
                                <!-- End of address field -->
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Cell No</label>
                                    <div class="col-md-2 col-sm-3">
                                        <select name="phone_code" class="form-control" required="">
                                            <option value="">Select Code</option>
                                            <option value="0300">0300</option>
                                            <option value="0301">0301</option>
                                            <option value="0302">0302</option>
                                            <option value="0303">0303</option>
                                            <option value="0304">0304</option>
                                            <option value="0305">0305</option>
                                            <option value="0306">0306</option>
                                            <option value="0307">0307</option>
                                            <option value="0308">0308</option>
                                            <option value="0309">0309</option>
                                            <option value="0310">0310</option>
                                            <option value="0311">0311</option>
                                            <option value="0312">0312</option>
                                            <option value="0313">0313</option>
                                            <option value="0314">0314</option>
                                            <option value="0315">0315</option>
                                            <option value="0320">0320</option>
                                            <option value="0321">0321</option>
                                            <option value="0322">0322</option>
                                            <option value="0323">0323</option>
                                            <option value="0324">0324</option>
                                            <option value="0325">0325</option>
                                            <option value="0331">0331</option>
                                            <option value="0332">0332</option>
                                            <option value="0333">0333</option>
                                            <option value="0334">0334</option>
                                            <option value="0335">0335</option>
                                            <option value="0336">0336</option>
                                            <option value="0340">0340</option>
                                            <option value="0341">0341</option>
                                            <option value="0342">0342</option>
                                            <option value="0343">0343</option>
                                            <option value="0344">0344</option>
                                            <option value="0345">0345</option>
                                            <option value="0346">0346</option>
                                            <option value="0347">0347</option>
                                        </select>
                                    </div>
                                    <div class="col-sm-4">
                                        <input  name="p_number" class="form-control"  id="p_number" type="text" required="" placeholder="It is Required">
                                    </div>
                                </div>
                                
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <input class="btn btn-default" type="submit" value="Add Dependent"/>
                                        
                                         <input class="btn btn-default" type="reset" value="Cancel"/>
                                    </div>
                                    
                                </div>
                            </form>
							<?php echo validation_errors();?>
                        </div>
                    </div>
                </div>
            </div>
       </div>
        <!-- /.col-lg-12 -->
        <!-- End Validation States -->
    </div>
</div>
