<div id="page-wrapper">

    <div class="page-content page-content-ease-in">

        <!-- begin PAGE TITLE ROW -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>Staff Management
                        <a href="<?php echo base_url('emp_medical_cont/addMedical/'.$records[0]->id);?>"><input type="submit" class="btn btn-default" value="Add Medical History"/></a>
                        <?php if(!isset($trash)){?>
                    <a href="<?php echo base_url('emp_medical_cont/load_deleted_medical/'.$records[0]->id);?>"><input class="btn btn-danger" type="submit" value="Trash"/></a>
                <?php }else{?>
                <a href="<?php echo base_url('emp_medical_cont/getEmpMedicalById/'.$records[0]->id);?>"><input class="btn btn-default" type="submit" value="Back"/></a>
                <?php }?>
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url('emp_emp_cont');?>">Staff Management</a>
                        </li>
                        <li class="active">Staff Medical</li>
                    </ol>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
           
        <!-- end PAGE TITLE ROW -->
        <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Employee Profile</h4>
                        </div>
                        <div class="clearfix">
                            
                        </div>
                    </div>
                    <div class="panel-collapse collapse in" id="validationExamples">
                        <div class="portlet-body">
                                <?php
                                if(isset($_SESSION['success'])&&isset($_SESSION['failed'])):    
                                if($_SESSION['success']==TRUE):?>
                    
                                    <div class="alert alert-success" role="alert">
                                        <span class="fa fa-thumbs-up" aria-hidden="true"></span>
                                        <span >Message: <?php echo $_SESSION['msg'];?></span>
                                       
                                    </div>
                                <?php unset($_SESSION['success']);
                                      unset($_SESSION['failure']);
                                    else: if($_SESSION['failure']==True):?>
                                    <div class="alert alert-danger" role="alert">
                                        <span class="fa fa-thumbs-down" aria-hidden="true"></span>
                                        <span >Message: <?php echo $_SESSION['msg'];?></span>
                                       
                                    </div>
                            <?php     unset($_SESSION['success']);
                                      unset($_SESSION['failure']);
                                    endif;endif;
                            endif;
                            ?>
                                <!-- Start of father's name name and number fields -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="number">Number</label>
                                    <div class="col-sm-4">
                                        <span><?php echo $records[0]->no;?></span>
                                        
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="name">Rank/Name</label>
                                    <div class="col-sm-4">
                                        <span><?php echo $records[0]->rank.' '.$records[0]->name;?></span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="name">Office</label>
                                    <div class="col-sm-4">
                                        <span><?php echo $records[0]->depname;?></span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- begin ADVANCED TABLES ROW -->
        <div class="row">

            <div class="col-lg-12">

                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Medical History</h4>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <div role="grid" class="dataTables_wrapper form-inline" id="example-table_wrapper">

                                <table class="table table-striped table-bordered table-hover table-green dataTable" id="example-table" aria-describedby="example-table_info">

                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width:auto;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                Sr.#
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Browser: activate to sort column ascending">
                                                Category Type
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Platform(s): activate to sort column ascending">
                                                Disease
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Engine version: activate to sort column ascending">
                                                From
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Engine version: activate to sort column ascending">
                                                To
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Engine version: activate to sort column ascending">
                                                Admitted CMH
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Engine version: activate to sort column ascending">
                                                Disposal
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Engine version: activate to sort column ascending">
                                                Review Date
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Engine version: activate to sort column ascending">
                                                Remarks
                                            </th>
                                            
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="CSS grade: activate to sort column ascending">
                                                Actions
                                            </th>
                                        </tr>
                                    </thead>

                                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                                    
                                        <?php $count=1;
                                            if($meds!=NULL):
                                                foreach ($meds as $row): ?>
                                                    <tr class="gradeA odd">
                                                        <td class="  sorting_1"><?php echo $count;?></td>
                                                        <td class=" "><?php echo $row->cat;?></td>
                                                        <td class=" "><?php echo $row->diseases;?></td>
                                                        <td class=" "><?php echo $row->from;?></td>
                                                        <td class=" "><?php echo $row->to;?></td>
                                                        <td class=" "><?php echo $row->admittedcmh;?></td>
                                                        <td class=" "><?php echo $row->disposal;?></td>
                                                        <td class=" "><?php echo $row->review;?></td>
                                                        <td class=" "><?php echo $row->remarks;?></td>

                                            
                                                        <?php if(!isset($trash)){?>
                                                        <td>    <div class="tooltip-demo">
                                                                <a href="<?php echo base_url('emp_medical_cont/updateMedical/'.$records[0]->id.'/'.$row->id);?>">
                                                                    <li class="fa fa-edit" data-original-title="Edit Medical History" data-placement="bottom" data-toggle="tooltip"></li></a>
                                                                <a href="<?php echo base_url('emp_medical_cont/deleteMedical/'.$records[0]->id.'/'.$row->id);?>">
                                                                    <li class="fa fa-trash-o" data-original-title="Delete Medical History" data-placement="bottom" data-toggle="tooltip"></li>
                                                                </a>
                                                            </div>
                                                        </td>
                                                        <?php } else{ ?>
                                                            
                                                            <td>
                                                                <a class="tooltip-demo" href="<?php echo base_url('emp_medical_cont/recover/'.$records[0]->id.'/'.$row->id);?>" >
                                                                    <li class="fa fa-undo" title="Recover" data-placement="bottom" data-toggle="tooltip">

                                                                    </li>
                                                                </a>

                                                            </td>
                                                        <?php }?>
                                                 
                                                    </tr>
                                        <?php $count++;
                                                endforeach;
                                            endif;?>
                                        
                                </tbody>
                            </table>

                        </div>
                        <!-- /.table-responsive -->
                    </div>
                    <!-- /.portlet-body -->
                </div>
                <!-- /.portlet -->

            </div>
            <!-- /.col-lg-12 -->

        </div>
        <!-- /.row -->

    </div>
    <!-- /.page-content -->

    </div>
</div>