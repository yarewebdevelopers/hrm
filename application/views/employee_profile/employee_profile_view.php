<div id="page-wrapper">


    <div class="page-content">

        <!-- begin PAGE TITLE ROW -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>Staff Management
                        
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url();?>/emp_emp_cont">Staff Management</a>
                        </li>
                        <li class="active">View Staff Profile</li>
                    </ol>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>

<?php //print_r($records);exit;?>
       <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>View Staff Profile</h4>
                        </div>
                        <div class="clearfix">
                            
                        </div>
                    </div>
                    <div class="panel-collapse collapse in" id="validationExamples">
                        <div class="portlet-body">
                            <form class="form-horizontal" id="validate" role="form" novalidate="novalidate">
                                
                                <!-- Start of father's name name and number fields -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="number">Number</label>
                                    <div class="col-sm-4">
                                        <input name="number" class="form-control" id="number" disabled type="text" value="<?php echo $records[0]->no;?>" placeholder=""/>
                                    </div>
                                
                                    <label class="col-sm-2 control-label" for="name">Name</label>
                                    <div class="col-sm-4">
                                        <input name="name" class="form-control" id="name" disabled type="text" value="<?php echo $records[0]->name;?>" placeholder=""/>
                                    </div>
                                </div>
                                
                                <div class='form-group'>                              
                                    <label class="col-sm-2 control-label" for="father">Father's Name</label>
                                    <div class="col-sm-4">
                                        <input name="father" class="form-control" disabled id="father" value="<?php echo $records[0]->fathername;?>" placeholder=""/>
                                    </div>
                                
                                    
                                    <label class="col-sm-2 control-label" for="cnic">CNIC</label>
                                    <div class="col-sm-4">
                                        <input name="cnic" class="form-control" disabled id="cnic" value="<?php echo $records[0]->cnicno;?>" disabled placeholder="It is Required"/>
                                    </div>
                                </div>  
                                        
                                <div class="form-group">     
                                    <label class="col-sm-2 control-label">Rank</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" value="<?php echo $records[0]->rank;?>" disabled/>
                                    </div>
                                
                                    
                                    
                                    <label class="col-sm-2 control-label" for="reporting">Reporting To</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" value="<?php echo $records[0]->repto;?>" disabled/>
                                    </div>
                                </div>
                                <!-- end of CNIC, rank and reporting to-->
                                    
                                <!--Start of DOB, DOJ, DOR fields-->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="dob">Date of Birth</label>
                                    <div id="sandbox-container" class="col-sm-4">
                                        <input class="form-control" value="<?php echo $records[0]->dob;?>" disabled/>
                                    </div>
                                
                                
                                    <label class="col-sm-2 control-label" for="doj">Date of Joining</label>
                                    <div class="col-sm-4" id="sandbox-container">
                                        <input class="form-control" value="<?php echo $records[0]->doj;?>" disabled/>
                                    </div>
                                </div>
                                <div class="form-group">
                                
                                    <label class="col-sm-2 control-label" for="doj" >Date of Retirement</label>
                                    <div class="col-sm-10 col-md-4" id="sandbox-container">
                                        <input class="form-control" value="<?php echo $records[0]->dor;?>" disabled/>
                                    </div>
                                
                                </div>
                                <!--end of DOB, DOJ, DOR fields-->
                                
                                <!--start of address and City Fields-->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="address">Address</label>
                                    <div class="col-sm-5">
                                        <input class="form-control" value="<?php echo $records[0]->address;?>" disabled/>
                                    </div>
                                    <label class="col-sm-1 control-label" for="address">City</label>
                                    <div class='col-sm-4'>    
                                        <input class="form-control" value="<?php echo $records[0]->city;?>" disabled/>
                                    </div>
                                </div>
                                
                                <!-- End of address field -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Caste</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" value="<?php echo $records[0]->cast;?>" disabled/>
                                    </div>
                                
                                    <label class="col-sm-2 control-label">Blood Group</label>
                                    <div class="col-sm-4">
                                        <input class="form-control" disabled value="<?php echo $records[0]->bloodgroup;?>"/>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Cell No</label>
                                    <div class="col-sm-4">
                                        <input  name="p_number" value="<?php echo $records[0]->cellno;?>"disabled class="form-control" id="p_number" type="text" required="" placeholder="It is Required">
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <div class="col-sm-10">
                                        <a href="<?php echo base_url('emp_family_cont/empFml');?>"><button class="btn btn-default">Family</button></a>
                                        <a href="<?php echo base_url('emp_csd_cont/empFml');?>"><button class="btn btn-default">CSD Procurement</button></a>
                                        <a href="<?php echo base_url('emp_csd_cont/empFml');?>"><button class="btn btn-default">Classes</button></a>
                                        <a href="<?php echo base_url('emp_leave_cont/getLeaveRecord');?>"><button class="btn btn-default">Leaves</button></a>
                                    </div>
                                    
                                </div>
                                
                            </form>
                        </div>
                    </div>
                </div>
            </div>
       </div>
        <!-- /.col-lg-12 -->
        <!-- End Validation States -->
    </div>
</div>
