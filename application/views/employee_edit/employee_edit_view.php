<div id="page-wrapper">

<?php //print_r($records);exit;?>
<?php //print_r($cities);exit;?>
    <div class="page-content">

        <!-- begin PAGE TITLE ROW -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>Staff Management
                        
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url();?>/emp_emp_cont">Staff Management</a>
                        </li>
                        <li class="active">Edit Staff</li>
                    </ol>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>


       <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Edit Staff Form</h4>
                        </div>
                        <div class="clearfix">
                            
                        </div>
                    </div>
                    <div class="error">
                    <?php if($this->uri->segment(4)){?>
                    	
                    	
                    	
                    	
                    	
                    
                        
                     <div class="alert alert-success" role="alert">
                                        <span class="fa fa-thumbs-up" aria-hidden="true"></span>
                                        <span class="sr-only">Message:</span>
                                        <?php echo "Updated Record Successfully";
 
                                        
                                        ?>
                                                                   
                                       
                                        
                                    </div>
                    <?php }?>
                    
                    </div>
                    
                    
                    
                    
                    <div class="panel-collapse collapse in" id="validationExamples">
                        <div class="portlet-body">

                            
                            <form class="form-horizontal" id="validate" role="form" novalidate="novalidate" method="POST" action="<?php echo base_url('emp_emp_cont/updateEmp/'.$records[0]->id);?>">

                                
                                <!-- Start of father's name name and number fields -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="no">Number</label>
                                    <div class="col-sm-4">
                                        <input name="no" class="form-control" id="no" value="<?php echo $records[0]->no;?>" required="" type="text" placeholder="It is Required"/>
                                    </div>
                                    
                                    
                                    <label class="col-sm-2 control-label" for="name">Name</label>
                                    <div class="col-sm-4">
                                        <input name="name" class="form-control" id="name" required="" value="<?php echo $records[0]->name;?>" type="text" placeholder="It is Required"/>
                                    </div>
                                </div>
                                
                                <div class='form-group'>                              
                                    <label class="col-sm-2 control-label" for="fathername" >Father's Name</label>
                                    <div class="col-sm-4">
                                        <input name="fathername" class="form-control" id="fathername" value="<?php echo $records[0]->fathername;?>" required="" placeholder="It is Required"/>
                                    </div>
                                                                    
                                    <label class="col-sm-2 control-label" for="cnic">CNIC</label>
                                    <div class="col-sm-4">

                                        <input name="cnicno" class="form-control" id="cnic" required="" value="<?php echo $records[0]->cnicno;?>" placeholder="It is Required"/>

                                    </div>
                                </div>  
                                        
                                <div class="form-group">     
                                    <label class="col-sm-2 control-label">Rank</label>
                                    <div class="col-sm-4">
                                      <select name="rankid" id="rankid" class="form-control" required="">
                                        <option value="">Select One</option>

                                          <?php foreach($ranks as $rank){
                                          if($records[0]->rankid==$rank->rankid){
                                          	
                                          	?>
                                          	<option selected="selected" value="<?php echo $records[0]->rankid; ?>"><?php echo $records[0]->rank;?></option>
                                          	<?php 
                                                
                                          } 
                                          else
                                              {?>
                                          
                                            <option value="<?php echo $rank->rankid;?>"><?php echo $rank->rank;?></option>
                                            
                                            <?php }
                                           }?>

                                        </select>                                   
                                         </div>
                                
                                    
                                    
                                    <label class="col-sm-2 control-label" for="repto">Reporting To</label>
                                    <div class="col-sm-4">
                                       <select name="repto" id="repto" class="form-control" required="">
                                        <option value="">Select One</option>
                                 
                                        
                                          <?php foreach($ranks as $rank){?>
                                          <?php if($records[0]->repto==$rank->rankid){
                                          	?>
                                          	<option selected="selected" value="<?php echo $rank->rankid;?>"><?php echo $rank->rank;?></option>
                                         
                                         <?php } else{ ?> 
                                          
                                         
                                            <option value="<?php echo $rank->rankid;?>"><?php echo $rank->rank;?></option>
                                            
                                            <?php } }?>

                                        </select>
                                    </div>
                                </div>
                                <!-- end of CNIC, rank and reporting to-->
                                    
                                <!--Start of DOB, DOJ, DOR fields-->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="dob">Date of Birth</label>
                                    <div  class="col-sm-4">
                                        <input type="text" name="dob" id="dob" class="form-control" required="" value="<?php echo $records[0]->dob;?>" placeholder="It is Required"/>
                                    </div>
                                
                                
                                    <label class="col-sm-2 control-label" for="doj">Date of Joining</label>
                                    <div class="col-sm-4" >
                                        <input name="doj" class="form-control" id="doj" required="" type="text" value="<?php echo $records[0]->doj;?>" placeholder="It is Required"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                
                                    <label class="col-sm-2 control-label" for="doj" >Date of Retirement</label>
                                    <div class="col-sm-10 col-md-4" >
                                        <input name="dor" class="form-control" id="dor" required="" type="text" value="<?php echo $records[0]->dor;?>" placeholder="It is Required"/>
                                    </div>
                                
                                </div>
                                <!--end of DOB, DOJ, DOR fields-->
                                
                                <!--start of address and City Fields-->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="address">Address</label>
                                    <div class="col-sm-5">
                                        <input name="address" class="form-control" id="address" required="" value="<?php echo $records[0]->address;?>" type="text" placeholder=""/>
                                    </div>
                                    <label class="col-sm-1 control-label" for="address">City</label>
                                    <div class='col-sm-4'>    
                                        <select name="city" class="form-control" required="">
                                        <option value="">Select</option>
                                        <?php foreach($cities as $city){
                                        	if($records[0]->city==$city->Name){
                                        		
                                        		?>
                                        		<option selected="selected" value="<?php echo $city->Name;?>"><?php echo $city->Name;?></option>
                                        		<?php }else{ ?>
                                        	
                                        
                                        	
                                        	
                                        
                                        <option value="<?php echo $city->Name;?>"><?php echo $city->Name;?></option>
                                        <?php } }?>

                                        </select>
                                    </div>
                                </div>
                                
                                <!-- End of address field -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Caste</label>
                                    <div class="col-sm-4">
                                       <select name="castid" class="form-control" required="">
                                            <option value="">Select One</option>
                                            
                                            <?php foreach($casts as $cast){
                                            
                                            	if($records[0]->castid==$cast->castid){
                                            	?>
                                            	<option selected="selected" value="<?php echo $records[0]->castid;?>"><?php echo $records[0]->cast;?></option>
                                            	<?php }else{?>
                                            <option value="<?php echo $cast->castid;?>"><?php echo $cast->cast;?></option>
                                            <?php } }?>
                                            
                                        </select>                                    </div>
                                
                                    <label class="col-sm-2 control-label">Blood Group</label>
                                    <div class="col-sm-4">
                                        <select name="bloodgroup" class="form-control" required="">
                                            <option value="">Select One:</option>
                                            <option selected="selected" value="<?php echo $records[0]->bloodgroup;?>" ><?php echo $records[0]->bloodgroup;?></option>
                                            <option value="AB+">AB+</option>
                                            <option value="AB-">AB-</option>
                                            <option value="A+">A+</option>
                                            <option value="A-">A-</option>
                                            <option value="B+">B+</option>
                                            <option value="B-">B-</option>
                                            <option value="O+">O+</option>
                                            <option value="O-">O-</option>
                                        </select>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Cell No</label>
                                    <div class="col-md-2 col-sm-3">
                                        <select name="phone_code" class="form-control" required="">
                                            <option value="">Select Carrier</option>
                                            <option selected="selected" value="<?php echo  substr($records[0]->cellno, 0,4);?>" ><?php echo  substr($records[0]->cellno, 0,4);?></option>
                                            <option value="0300">0300</option>
                                            <option value="0301">0301</option>
                                            <option value="0302">0302</option>
                                            <option value="0303">0303</option>
                                            <option value="0304">0304</option>
                                            <option value="0305">0305</option>
                                            <option value="0306">0306</option>
                                            <option value="0307">0307</option>
                                            <option value="0308">0308</option>
                                            <option value="0309">0309</option>
                                            <option value="0310">0310</option>
                                            <option value="0311">0311</option>
                                            <option value="0312">0312</option>
                                            <option value="0313">0313</option>
                                            <option value="0314">0314</option>
                                            <option value="0315">0315</option>
                                            <option value="0320">0320</option>
                                            <option value="0321">0321</option>
                                            <option value="0322">0322</option>
                                            <option value="0323">0323</option>
                                            <option value="0324">0324</option>
                                            <option value="0325">0325</option>
                                            <option value="0331">0331</option>
                                            <option value="0332">0332</option>
                                            <option value="0333">0333</option>
                                            <option value="0334">0334</option>
                                            <option value="0335">0335</option>
                                            <option value="0336">0336</option>
                                            <option value="0340">0340</option>
                                            <option value="0341">0341</option>
                                            <option value="0342">0342</option>
                                            <option value="0343">0343</option>
                                            <option value="0344">0344</option>
                                            <option value="0345">0345</option>
                                            <option value="0346">0346</option>
                                            <option value="0347">0347</option>
                                        </select>
                                    </div>
<<<<<<< HEAD
                                    <div class="col-sm-4">
                                        <input  name="p_number" class="form-control" value="<?php echo substr($records[0]->cellno,4);?>" id="p_number" type="text" required="" placeholder="It is Required">
=======
                                    <div class="col-sm-3">
                                        <input  name="cellno" class="form-control" value="<?php echo substr($records[0]->cellno,4);?>" id="p_number" type="text" required="" placeholder="It is Required">
>>>>>>> 23ab96946fd91db7737fbfef6edf539b1ed8e6ff
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Add Image</label>
                                    <div class="col-sm-6">
                                        <input type="file" name="image"/>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                    <input type="hidden" name="depid" id="depid" value="1" />
                                    <?php //print_r($records);?>
                                    <input type="hidden" name="eid" id="eid" value="<?php echo $records[0]->id;?>"/>
                                        <input class="btn btn-default" name="submit" type="submit" value="Update"/>
                                        
                                         <input class="btn btn-default" type="reset" value="Cancel"/>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
       </div>
        <!-- /.col-lg-12 -->
        <!-- End Validation States -->
    </div>
</div>
