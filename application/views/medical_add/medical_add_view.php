<div id="page-wrapper">


    <div class="page-content">

        <!-- begin PAGE TITLE ROW -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>Staff Management
                        
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url('emp_emp_cont');?>">Staff Management</a>
                        </li>
                        <li class="active">Add Medical Record</li>
                    </ol>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>


       <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Add Medical Record Form</h4>
                        </div>
                        <div class="clearfix">
                            
                        </div>
                    </div>
                    <div class="panel-collapse collapse in" id="validationExamples">
                        <div class="portlet-body">
                            <form class="form-horizontal" id="validate" role="form" novalidate="novalidate" method="POST">
                                
                                <!-- Start of father's name name and number fields -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="number">Number</label>
                                    <div class="col-sm-4">
                                        <input name="number" class="form-control" id="number" disabled type="text" placeholder="It is Required" value="<?php echo $records[0]->no;?>"/>
                                    </div>
                                
                                    <label class="col-sm-2 control-label" for="name">Name</label>
                                    <div class="col-sm-4">
                                        <input name="name" class="form-control" id="name" disabled type="text" placeholder="It is Required" value="<?php echo $records[0]->name;?>"/>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="address">Disease</label>
                                    <div class="col-sm-4">
                                        <input name="diseases" class="form-control" id="diseases" required="" type="text" placeholder=""/>
                                    </div>
                                    <label class="col-sm-2 control-label" for="address">Disposal</label>
                                    <div class='col-sm-4'>    
                                        <input name="disposal" class="form-control" id="disposal" required="" type="text" placeholder="It is Required"/>
                                    </div>
                                </div> 
                                        
                                <div class="form-group">     
                                    <label class="col-sm-2 control-label">Category</label>
                                    <div class="col-sm-4">
                                        <select name="cat" class="form-control" required="">
                                            <option value="">Select One:</option>
                                            <option value="Category A">Category A</option>
                                            <option value="Category B">Category B</option>
                                            <option value="Category C">Category C</option>
                                            
                                        </select>
                                    </div>
                                
                                    
                                    
                                    <label class="col-sm-2 control-label" for="reporting">Admitted CMH</label>
                                    <div class="col-sm-4">
                                        <select name="cmh" class="form-control" required="">
                                            
                                            <option value="">Select One:</option>
                                            <option value="Yes">Yes</option>
                                            <option value="No">No</option>
                                            
                                            </select>
                                    </div>
                                </div>
                                <!-- end of CNIC, rank and reporting to-->
                                    
                                <!--Start of DOB, DOJ, DOR fields-->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="dob">Admitted From</label>
                                    <div id="sandbox-container" class="col-sm-4">
                                        <input type="text" name="from" id="from" class="form-control" required="" placeholder="It is Required"/>
                                    </div>
                                
                                
                                    <label class="col-sm-2 control-label" for="doj">Admitted To</label>
                                    <div class="col-sm-4" id="sandbox-container">
                                        <input name="to" class="form-control" id="to" required="" type="text" placeholder="It is Required"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                
                                    <label class="col-sm-2 control-label" for="doj" >Review Date</label>
                                    <div class="col-sm-10 col-md-4" id="sandbox-container">
                                        <input name="review" class="form-control" id="review" required="" type="text" placeholder="It is Required"/>
                                    </div>
                                
                                    <label class="col-sm-2 control-label">Partiio</label>
                                    <div class="col-sm-4">
                                        <input  name="partiio" class="form-control" id="partiio" required="" type="text" placeholder="It is Required"/>
                                    </div>
                                </div>
                                <!--end of DOB, DOJ, DOR fields-->
                                
                                <!--start of address and City Fields-->
                                
                                
                                <!-- End of address field -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label">Remarks</label>
                                    <div class="col-sm-4">
                                        <textarea  rows="3" name="remarks" class="form-control" id="remarks" required="" type="text" placeholder="It is Required"></textarea>
                                    </div>
                                    
                                
                                    
                                </div>
                                             
                                                             
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <input class="btn btn-default" type="submit" name="submit" value="Add Medical History"/>
                                        
                                         <input class="btn btn-default" type="reset" value="Cancel"/>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
       </div>
        <!-- /.col-lg-12 -->
        <!-- End Validation States -->
    </div>
</div>
