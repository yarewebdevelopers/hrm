<div id="page-wrapper">

<div class="page-content page-content-ease-in">

    <!-- begin PAGE TITLE ROW -->
    <div class="row">
        <div class="col-lg-12">
            <div class="page-title">
                <h1>Setup Entries
                    
                </h1>
                <ol class="breadcrumb">
                    <li><i class="fa fa-dashboard"></i>  <a href="index.html">Setup Entries</a>
                    </li>
                    <li class="active">Caste</li>
                </ol>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- end PAGE TITLE ROW -->

    <!-- begin ADVANCED TABLES ROW -->
    <div class="row">

        <div class="col-lg-12">

            <div class="portlet portlet-default">
                <div class="portlet-heading">
                    <div class="portlet-title">
                        <h4>Attachments</h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <div role="grid" class="dataTables_wrapper form-inline" id="example-table_wrapper">

                            <table class="table table-striped table-bordered table-hover table-green dataTable" id="example-table" aria-describedby="example-table_info">

                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width:auto;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                            Sr.#
                                        </th>
                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Browser: activate to sort column ascending">
                                            Caste Name
                                        </th>
                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Platform(s): activate to sort column ascending">
                                            No of People
                                        </th>
                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="CSS grade: activate to sort column ascending">
                                            Actions
                                        </th>
                                    </tr>
                                </thead>

                                <tbody role="alert" aria-live="polite" aria-relevant="all">
                                    <tr class="gradeA odd">
                                        <td class="  sorting_1">1</td>
                                        <td class=" ">ABC</td>
                                        <td class=" ">100</td>
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA even">
                                        <td class="  sorting_1">2</td>

                                        <td class=" ">TUV</td>
                                        <td class="center ">10</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA odd">
                                        <td class="  sorting_1">3</td>

                                        <td class=" ">XYZ</td>
                                        <td class="center ">150</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA even">
                                        <td class="  sorting_1">4</td>

                                        <td class=" ">ABC</td>
                                        <td class="center ">13</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA odd">
                                        <td class="  sorting_1">5</td>

                                        <td class=" ">TUV</td>
                                        <td class="center ">6</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA even">
                                        <td class="  sorting_1">6</td>

                                        <td class=" ">XYZ</td>
                                        <td class="center ">120</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA odd">
                                        <td class="  sorting_1">7</td>

                                        <td class=" ">ABC</td>
                                        <td class="center ">10</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA even">
                                        <td class="  sorting_1">8</td>

                                        <td class=" ">TUV</td>
                                        <td class="center ">9</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA odd">
                                        <td class="  sorting_1">9</td>

                                        <td class=" ">XYZ</td>
                                        <td class="center ">150</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA even">
                                        <td class="  sorting_1">10</td>

                                        <td class=" ">ABC</td>
                                        <td class="center ">20</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA even">
                                        <td class="  sorting_1">11</td>

                                        <td class=" ">TUV</td>
                                        <td class="center ">9</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA even">
                                        <td class="  sorting_1">12</td>

                                        <td class=" ">XYZ</td>
                                        <td class="center ">160</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                        </td>
                                    </tr>
                            </tbody>
                        </table>

                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.portlet-body -->
            </div>
            <!-- /.portlet -->

        </div>
        <!-- /.col-lg-12 -->

    </div>
    <!-- /.row -->

</div>
<!-- /.page-content -->

</div>