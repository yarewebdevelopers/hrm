<div id="page-wrapper">

    <div class="page-content page-content-ease-in">

        <!-- begin PAGE TITLE ROW -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>Staff Management
                        <a href="<?php echo base_url('emp_leave_cont/addLeave/'.$empid);?>"><input type="submit" class="btn btn-default" value="Add Leave"/></a>
                        <a href="<?php echo base_url('emp_leave_cont/addLeave/'.$empid);?>"><input type="submit" class="btn btn-danger" value="Trash"/></a>
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url('/emp_emp_cont');?>">Staff Management</a>
                        </li>
                        <li class="active">Staff Leaves</li>
                    </ol>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <!-- end PAGE TITLE ROW -->
        <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Employee Profile</h4>
                        </div>
                        <div class="clearfix">
                            
                        </div>
                    </div>
                    <div class="panel-collapse collapse in" id="validationExamples">
                        <div class="portlet-body">
                                
                                <!-- Start of father's name name and number fields -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="number">Number</label>
                                    <div class="col-sm-4">
                                        <span></span>
                                        
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="name">Rank/Name</label>
                                    <div class="col-sm-4">
                                        <span></span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="name">Office</label>
                                    <div class="col-sm-4">
                                        <span></span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- begin LEAVES ROW -->
        <div class="row">

            <div class="col-lg-12">

                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Staff Leaves</h4>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <div role="grid" class="dataTables_wrapper form-inline" id="example-table_wrapper">

                                <table class="table table-striped table-bordered table-hover table-green dataTable" id="example-table" aria-describedby="example-table_info">

                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width:auto;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                Sr.#
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Engine version: activate to sort column ascending">
                                                Leave From
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Engine version: activate to sort column ascending">
                                                Leave Till
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Engine version: activate to sort column ascending">
                                                Duration
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Engine version: activate to sort column ascending">
                                                Reason
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="CSS grade: activate to sort column ascending">
                                                Actions
                                            </th>
                                        </tr>
                                    </thead>

                                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                                    <?php if(isset($records)){?>
                                    <?php foreach($records as $record){?>
                                        <tr class="gradeA odd">
                                            <td class="  sorting_1"><?php echo $record->id;?></td>
                                            <td class="center "><?php echo  $record->from;?></td>
                                            <td class="center "><?php echo $record->to;?></td>
                                            <td class="center "><?php echo $record->reason;?></td>
                                            <td class="center "><?php echo $record->remarks;?></td>
                                            <td class="center ">
                                                <a href="<?php echo base_url('emp_leave_cont/updateLeave/'.$record->eid.'/'.$record->id);?>"><li class="fa fa-edit"></li></a>
                                                
                                               <a href="<?php echo base_url('emp_leave_cont/deleteLeave/'.$record->eid.'/'.$record->id);?>"> <li class="fa fa-trash-o"></li></a>
                                            </td>
                                        </tr>
                                        <?php }}?>
                                                                          </tbody>
                                </table>

                            </div>
                        <!-- /.table-responsive -->
                        </div>
                    <!-- /.portlet-body -->
                    </div>
                <!-- /.portlet -->

                </div>
            <!-- /.col-lg-12 -->

            </div>
        <!-- /.row -->

        </div>
    <!-- /.page-content -->
    </div>
</div>
