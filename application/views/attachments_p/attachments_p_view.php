<div id="page-wrapper">
<?php //print_r($employee);exit;?>
<?php //print_r($records);exit;?>

    <div class="page-content">
    

        <!-- begin PAGE TITLE ROW -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>Staff Management
                        
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i>  <a href="index.html">Staff Management</a>
                        </li>
                        <li class="active">Attachment View</li>
                    </ol>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>


       <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Attachment View Form</h4>
                        </div>
                        <div class="clearfix">
                            
                        </div>
                    </div>
                    <div class="panel-collapse collapse in" id="validationExamples">
                        <div class="portlet-body">
                            <form class="form-horizontal" id="validate" role="form" novalidate="novalidate">
                                
                                <!-- Start of father's name name and number fields -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="number">Number</label>
                                    <div class="col-sm-4">
                                        <input name="number" class="form-control" id="number" disabled="" type="text" value="<?php echo $employee[0]->no;?>"/>
                                    </div>
                                
                                    <label class="col-sm-2 control-label" for="name">Name</label>
                                    <div class="col-sm-4">
                                        <input name="name" class="form-control" id="name" disabled="" type="text" value="<?php echo $employee[0]->name;?>"/>
                                    </div>
                                </div>
                                
                                <div class='form-group'>                              
                                
                                    <label class="col-sm-2 control-label" for="rank">Rank</label>
                                    <div class="col-sm-4">
                                        <input name="rank" class="form-control" id="rank" disabled="" type="text" value="<?php echo $employee[0]->rank;?>"/>
                                    </div>
                                
                                    
                                    <label class="col-sm-2 control-label" for="cnic">Attached With</label>
                                    <div class="col-sm-4">
                                        <input name="attach" class="form-control" disabled="" id="attach" required="" placeholder="It is Required" value="<?php echo $records[0]->attwith;?>"/>
                                    </div>
                                </div>  
                                                                    
                                <!--Start of DOB, DOJ, DOR fields-->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="dob">Attached From</label>
                                    <div id="sandbox-container" class="col-sm-4">
                                        <input type="text" disabled="" name="attach_from" id="attach_from" class="form-control" required="" placeholder="It is Required" value="<?php echo $records[0]->from;?>"/>
                                    </div>
                                
                                
                                     <label class="col-sm-2 control-label" for="doj">Attached To</label>
                                    <div class="col-sm-4" id="sandbox-container">
                                        <input name="attach_to" class="form-control" disabled="" id="attach_to" required="" type="text" value="<?php echo $records[0]->to;?>" placeholder="It is Required"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                
                                    <label class="col-sm-2 control-label" for="doj" >Release Date</label>
                                    <div class="col-sm-4" id="sandbox-container">
                                        <input name="release" class="form-control" disabled="" id="release" required="" type="text" value="<?php echo $records[0]->reldate;?>" placeholder="It is Required"/>
                                    </div>
                                    <label class="col-sm-2 control-label" for="doj" >Duration</label>
                                    <div class="col-sm-4" id="sandbox-container">
                                        <input name="attach_duration" class="form-control" disabled="" id="attach_duration" required="" value="<?php echo $records[0]->runningdays;?>" type="text" placeholder="It is Required"/>
                                    </div>
                                
                                </div>
                                <!--end of DOB, DOJ, DOR fields-->
                                                                                               
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">

                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
       </div>
        <!-- /.col-lg-12 -->
        <!-- End Validation States -->
    </div>
</div>
