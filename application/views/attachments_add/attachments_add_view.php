
<?php print_r($records); ?>

<div id="page-wrapper">


    <div class="page-content">

        <!-- begin PAGE TITLE ROW -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>Staff Management
                        
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i>  <a href="index.html">Staff Management</a>
                        </li>
                        <li class="active">Add Attachments</li>
                    </ol>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>


       <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Add Attachment Form</h4>
                        </div>
                        <div class="clearfix">
                            
                        </div>
                    </div>
                    <div class="panel-collapse collapse in" id="validationExamples">
                        <div class="portlet-body">
                            <form class="form-horizontal" id="validate" role="form" novalidate="novalidate" method="post" action="<?php echo base_url('emp_att_cont/addAtt/'.$records[0]->id);?>">
                                
                                <!-- Start of father's name name and number fields -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="no">Number</label>
                                    <div class="col-sm-4">
                                        <input name="no" class="form-control" id="no" disabled="" value="<?php echo $records[0]->no;?>" type="text"/>
                                    </div>
                                
                                    <label class="col-sm-2 control-label" for="name">Name</label>
                                    <div class="col-sm-4">
                                        <input name="name" class="form-control" id="name" value="<?php echo $records[0]->name;?>" disabled="" type="text"/>
                                    </div>
                                </div>
                                
                                <div class='form-group'>                              
                                
                                    <label class="col-sm-2 control-label" for="rank">Rank</label>
                                    <div class="col-sm-4">
                                        <input name="rank" class="form-control" id="rank"  value="<?php echo $records[0]->rank;?>" disabled=""type="text"/>
                                    </div>
                                
                                    
                                    <label class="col-sm-2 control-label" for="attwith">Attached With</label>
                                    <div class="col-sm-4">
                                        <input name="attwith" class="form-control" id="attwith" required="" placeholder="It is Required"/>
                                    </div>
                                </div>  
                                                                    
                                <!--Start of DOB, DOJ, DOR fields-->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="from">Attached From</label>
                                    <div id="sandbox-container" class="col-sm-4">
                                        <input type="text" name="from" id="from" class="form-control" required="" placeholder="It is Required"/>
                                    </div>
                                
                                
                                    <label class="col-sm-2 control-label" for="to">Attached To</label>
                                    <div class="col-sm-4" id="sandbox-container">
                                        <input name="to" class="form-control" id="to" required="" type="text" placeholder="It is Required"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                
                                    <label class="col-sm-2 control-label" for="reldate" >Release Date</label>
                                    <div class="col-sm-4" id="sandbox-container">
                                        <input name="reldate" class="form-control" id="reldate" required="" type="text" placeholder="It is Required"/>
                                    </div>
                                    <label class="col-sm-2 control-label" for="runningdays" >Duration</label>
                                    <div class="col-sm-4">
                                        <input name="runningdays" class="form-control" id="runningdays" required="" type="text" placeholder="It is Required"/>
                                    </div>
                                
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="remarks" >Remarks</label>
                                    <div class="col-sm-4" >
                                        <textarea name="remarks" class="form-control" id="remarks" rows=3 required="" type="text" placeholder="It is Required"></textarea>
                                    </div>
                                
                                </div>
                                <!--end of DOB, DOJ, DOR fields-->
                                                                                               
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <input class="btn btn-default" type="submit" name="submit" value="Create Attachment"/>
                                        
                                         <input class="btn btn-default" type="reset" value="Cancel"/>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
       </div>
        <!-- /.col-lg-12 -->
        <!-- End Validation States -->
    </div>
</div>
