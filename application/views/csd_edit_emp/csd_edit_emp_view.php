<div id="page-wrapper">


    <div class="page-content">

        <!-- begin PAGE TITLE ROW -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>Staff Management
                        
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i>  <a href="index.html">Staff Management</a>
                        </li>
                        <li class="active">Edit CSD Procurement</li>
                    </ol>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>


       <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Edit CSD Procurement Form</h4>
                        </div>
                        <div class="clearfix">
                            
                        </div>
                    </div>
                    <div class="panel-collapse collapse in" id="validationExamples">
                        <div class="portlet-body">
                            <form class="form-horizontal" id="validate" role="form" novalidate="novalidate" method="post" action="<?php echo base_url('emp_csd_cont/UpdateCsd/'.$this->uri->segment(4));?>">
                                
                                <!-- Start of father's name name and number fields -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="number">Number</label>
                                    <div class="col-sm-4">
                                        <input name="number" class="form-control" id="number" disabled="" value="<?php echo $records[0]->no?>" type="text"/>
                                    </div>
                                
                                    <label class="col-sm-2 control-label" for="name">Name</label>
                                    <div class="col-sm-4">
                                        <input name="name" class="form-control" id="name" disabled="" value="<?php echo $records[0]->name?>" type="text"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="number">Rank</label>
                                    <div class="col-sm-4">
                                        <input name="rank" class="form-control" id="rank" disabled="" value="<?php echo $records[0]->rank?>" type="text"/>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="items">Item Name</label>
                                    <div class="col-sm-4">
                                        <input name="items" class="form-control" id="items" value="<?php echo $csdProcurement[0]->items?>" required="" type="text"/>
                                    </div>
                                    
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="installments">Installments Paid</label>
                                    <div class="col-sm-4">
                                        <input name="installmentspaid" class="form-control" id="installmentspaid" required="" value="<?php echo $csdProcurement[0]->installmentspaid?>" type="number"/>
                                    </div>
                                    <label class="col-sm-2 control-label" for="name">Installment Date</label>
                                    <div class="col-sm-4" id="sandbox-container">
                                        <input name="installment_date" class="form-control" id="installment_date" required="" value="<?php echo $csdProcurement[0]->installmentdate?>" type="text"/>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="installmentspending">Installments Pending</label>
                                    <div class="col-sm-4">
                                        <input name="installmentspending" class="form-control" id="installmentspending" required="" value="<?php echo $csdProcurement[0]->installmentspending?>" type="number" min="1" />
                                    </div>
                                    <label class="col-sm-2 control-label" for="installmentsdelayed">Installments Delayed</label>
                                    <div class="col-sm-4">
                                        <input name="installmentsdelayed" class="form-control" id="installmentsdelayed" required="" value="<?php echo $csdProcurement[0]->installmentsdelayed?>" type="number"/>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="remarks">Remarks</label>
                                    <div class="col-sm-4">
                                        <textarea name="remarks" class="form-control" id="remarks" type="text" rows="3"><?php echo $csdProcurement[0]->remarks?></textarea>
                                    </div>
                                </div>
                                
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <input class="btn btn-default" type="submit" name = "submit" value="Edit Pocurement"/>
                                        
                                         <input class="btn btn-default" type="reset" value="Cancel"/>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
       </div>
        <!-- /.col-lg-12 -->
        <!-- End Validation States -->
    </div>
</div>
