<div id="page-wrapper">
<?php //print_r($employee);exit;?>
<?php //print_r($records);exit;?>

    <div class="page-content">

        <!-- begin PAGE TITLE ROW -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>Staff Management
                        
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i>  <a href="index.html">Staff Management</a>
                        </li>
                        <li class="active">Edit Attachments</li>
                    </ol>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
          <div class="error">
                    <?php if($this->uri->segment(5)=='success'){?>
                    	
                    	
                    	
                    	
                    	
                    
                        
                     <div class="alert alert-success" role="alert">
                                        <span class="fa fa-thumbs-up" aria-hidden="true"></span>
                                        <span class="sr-only">Message:</span>
                                        <?php echo 'Record Updated Successfully';
 
                                        
                                        ?>
                                                                   
                                       
                                        
                                    </div>
                    <?php }?>
                    
                    </div>
        


       <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Edit Attachment Form</h4>
                        </div>
                        <div class="clearfix">
                            
                        </div>
                    </div>
                    <div class="panel-collapse collapse in" id="validationExamples">
                        <div class="portlet-body">
                            <form  method="post" action="<?php echo base_url('emp_att_cont/updateAtt/'.$records[0]->id); ?>"class="form-horizontal" id="validate" role="form" novalidate="novalidate">
                                
                                <!-- Start of father's name name and number fields -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="number">Number</label>
                                    <div class="col-sm-4">
                                        <input name="number" class="form-control" id="number" disabled="" type="text" value="<?php echo $employee[0]->no;?>"/>
                                    </div>
                                
                                    <label class="col-sm-2 control-label" for="name">Name</label>
                                    <div class="col-sm-4">
                                        <input name="name" class="form-control" id="name" disabled="" type="text" value="<?php echo $employee[0]->name;?>"/>
                                    </div>
                                </div>
                                
                                <div class='form-group'>                              
                                
                                    <label class="col-sm-2 control-label" for="rank">Rank</label>
                                    <div class="col-sm-4">
                                        <input name="rank" class="form-control" id="rank" disabled="" type="text" value="<?php echo $employee[0]->rank;?>"/>
                                    </div>
                                
                                    
                                    <label class="col-sm-2 control-label" for="cnic">Attached With</label>
                                    <div class="col-sm-4">
                                        <input name="attwith" class="form-control" id="attwith" required="" placeholder="It is Required" value="<?php echo $records[0]->attwith;?>"/>
                                    </div>
                                </div>  
                                                                    
                                <!--Start of DOB, DOJ, DOR fields-->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="from">Attached From</label>
                                    <div id="sandbox-container" class="col-sm-4">
                                        <input type="text" name="from" id="from" class="form-control" required="" placeholder="It is Required" value="<?php echo $records[0]->from;?>"/>
                                    </div>
                                
                                
                                    <label class="col-sm-2 control-label" for="to">Attached To</label>
                                    <div class="col-sm-4" id="sandbox-container">
                                        <input name="to" class="form-control" id="to" required="" type="text" value="<?php echo $records[0]->to;?>" placeholder="It is Required"/>
                                    </div>
                                </div>
                                <div class="form-group">
                                
                                    <label class="col-sm-2 control-label" for="reldate" >Release Date</label>
                                    <div class="col-sm-4" id="sandbox-container">
                                        <input name="reldate" class="form-control" id="reldate" required="" type="text" value="<?php echo $records[0]->reldate;?>" placeholder="It is Required"/>
                                    </div>
                                    <label class="col-sm-2 control-label" for="runningdays" >Duration</label>
                                    <div class="col-sm-4">
                                        <input name="runningdays" class="form-control" id="runningdays" required="" value="<?php echo $records[0]->runningdays;?>" type="text" placeholder="It is Required"/>
                                    </div>
                                    
                                
                                </div>
                                  <div class="form-group">
                                    <label class="col-sm-2 control-label" for="remarks" >Remarks</label>
                                    <div class="col-sm-4" >
                                        <textarea name="remarks" class="form-control" id="remarks" rows=3 required="" type="text" placeholder="It is Required"><?php echo $records[0]->remarks;?></textarea>
                                    </div>
                                
                                </div>
                                <!--end of DOB, DOJ, DOR fields-->
                                                                                               
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                    <input type="hidden" id="id" name="id" value="<?php echo $records[0]->id;?>"/>
                                    <input type="hidden" id="eid" name="eid" value="<?php echo $employee[0]->id;?>"/>
                                        <input class="btn btn-default" id="submit" name="submit" type="submit" value="Update Attachment"/>
                                        
                                         <input class="btn btn-default" type="reset" value="Cancel"/>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
       </div>
        <!-- /.col-lg-12 -->
        <!-- End Validation States -->
    </div>
</div>
