<div id="page-wrapper">

    <div class="page-content page-content-ease-in">

        <!-- begin PAGE TITLE AREA -->
        <!-- Use this section for each page's title and breadcrumb layout. In this example a date range picker is included within the breadcrumb. -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>Dashboard
                        <small>Content Overview</small>
                    </h1>

                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <!-- end PAGE TITLE AREA -->

    </div>
    <!-- /.page-content -->

</div>