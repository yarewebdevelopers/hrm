<div id="page-wrapper">


    <div class="page-content">

        <!-- begin PAGE TITLE ROW -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>Staff Management
                        <a href="<?php echo base_url('emp_award_cont/addEmpAward/'.$records[0]->id);?>"><input class="btn btn-default" type="submit" value="Add Staff Awards"/></a>
                        <?php if(!isset($trash)){?>
                        <a href="<?php echo base_url('emp_award_cont/load_deleted_awards/'.$records[0]->id)?>"><input class="btn btn-danger" type="submit" value="Trash"/></a>
                        <?php }else{?>
                        <a href="<?php echo base_url('emp_emp_cont/')?>"><input class="btn btn-default" type="submit" value="Back"/></a>
                        <?php }?>

                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i>  <a href="index.html">Staff Management</a>
                        </li>
                        <li class="active">Add Staff Award</li>
                    </ol>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>


       <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Add Staff Award</h4>
                        </div>
                        <div class="clearfix">
                            
                        </div>
                    </div>
                    <div class="panel-collapse collapse in" id="validationExamples">
                        <div class="portlet-body">
                            <form class="form-horizontal" id="validate" role="form" novalidate="novalidate" method="POST" action="<?php echo base_url('emp_award_cont/UpdateEmpAward/'.$records[0]->id.'/'.$awardss[0]->id);?>">
                                
                                <!-- Start of father's name name and number fields -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="number">Number</label>
                                    <div class="col-sm-4">
                                        <input name="number" class="form-control" id="number" disabled="" value="<?php echo $records[0]->no;?>" type="text"/>
                                    </div>
                                
                                    <label class="col-sm-2 control-label" for="name">Name</label>
                                    <div class="col-sm-4">
                                        <input name="name" class="form-control" id="name" disabled="" value="<?php echo $records[0]->name;?>" type="text"/>
                                    </div>
                                </div>
                                
                                <div class='form-group'>                              
                                
                                    <label class="col-sm-2 control-label" for="rank">Rank</label>
                                    <div class="col-sm-4">
                                        <input name="rank" class="form-control" id="rank" disabled="" type="text" value="<?php echo $records[0]->rank;?>"/>
                                    </div>
                                </div>
                                
                                <div class="form-group">    
                                    <label class="col-sm-2 control-label" for="award">Award Name</label>
                                    <div class="col-sm-4">
                                        <input name="award" class="form-control" id="award" required="" placeholder="It is Required" value="<?php echo $awardss[0]->award;?>"/>
                                    </div>
                                 
                                                                    
                                <!--Start of DOB, DOJ, DOR fields-->
                                
                                    <label class="col-sm-2 control-label" for="dob">Remarks</label>
                                    <div class="col-sm-4">
                                        <textarea type="text" rows="3" name="remarks" id="remarks" class="form-control" required="" placeholder="It is Required"><?php echo $awardss[0]->remarks;?></textarea>
                                    </div>
                                </div> 
                                
                           
                                
                                                                                               
                                <div class="form-group">
                                    <label class="col-sm-2 control-label"></label>
                                    <div class="col-sm-10">
                                        <input class="btn btn-default" type="submit" name="submit" value="Edit Award"/>
                                        
                                         <input class="btn btn-default" type="reset" value="Cancel"/>
                                    </div>
                                    
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
       </div>
        <!-- /.col-lg-12 -->
        <!-- End Validation States -->
    </div>
</div>
