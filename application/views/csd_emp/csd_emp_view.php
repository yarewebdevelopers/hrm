<div id="page-wrapper">

    <div class="page-content page-content-ease-in">

        <!-- begin PAGE TITLE ROW -->
        <div class="row">
            <div class="col-lg-12">
                <div class="page-title">
                    <h1>Staff Management
                        <a href="<?php echo base_url('emp_csd_cont/AddCsd/'.$this->uri->segment(3));?>"><button class="btn btn-default" >ADD CSD Item</button></a>
                        <a href="<?php echo base_url('emp_csd_cont/AddCsd/'.$this->uri->segment(3));?>"><button class="btn btn-danger" >Trash</button></a>
                    </h1>
                    <ol class="breadcrumb">
                        <li><i class="fa fa-dashboard"></i>  <a href="<?php echo base_url('emp_emp_cont')?>">Staff Management</a>
                        </li>
                        <li class="active">CSD Staff Procurement</li>
                    </ol>
                </div>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- /.row -->
        <!-- end PAGE TITLE ROW -->
    <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Employee Profile</h4>
                        </div>
                        <div class="clearfix">
                            
                        </div>
                    </div>
                    <div class="panel-collapse collapse in" id="validationExamples">
                        <div class="portlet-body">
                                
                                <!-- Start of father's name name and number fields -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="number">Number</label>
                                    <div class="col-sm-4">
                                        <span></span>
                                        
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="name">Rank/Name</label>
                                    <div class="col-sm-4">
                                        <span></span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="name">Office</label>
                                    <div class="col-sm-4">
                                        <span></span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- begin ADVANCED TABLES ROW -->
        <div class="row">

            <div class="col-lg-12">

                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Staff CSD Procurement</h4>
                        </div>
                        <div class="clearfix"></div>
                    </div>
                    <div class="portlet-body">
                        <div class="table-responsive">
                            <div role="grid" class="dataTables_wrapper form-inline" id="example-table_wrapper">

                                <table class="table table-striped table-bordered table-hover table-green dataTable" id="example-table" aria-describedby="example-table_info">

                                    <thead>
                                        <tr role="row">
                                            <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width:auto;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                                Sr.#
                                            </th>
                                            
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Platform(s): activate to sort column ascending">
                                                Item Name
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Platform(s): activate to sort column ascending">
                                                Installments
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Platform(s): activate to sort column ascending">
                                                Installments Pending
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Platform(s): activate to sort column ascending">
                                                Installments Delayed
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Platform(s): activate to sort column ascending">
                                                Installments Date
                                            </th>
                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Platform(s): activate to sort column ascending">
                                                Remarks
                                            </th>

                                            <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="CSS grade: activate to sort column ascending">
                                                Actions
                                            </th>
                                        </tr>
                                    </thead>

                                    <tbody role="alert" aria-live="polite" aria-relevant="all">
                                        <?php $count =1;
                                            if($records!=NULL):
                                                foreach($records as $row):?>
                                                    <tr class="gradeA odd">
                                                        <td class="  sorting_1"><?php echo $count;?></td>
                                                        <td class=" "><?php echo $row->items;?></td>
                                                        <td class=" "><?php echo $row->installmentspaid;?></td>
                                                        <td class=" "><?php echo $row->installmentspending;?></td>
                                                        <td class=" "><?php echo $row->installmentsdelayed;?></td>
                                                        <td class=" "><?php echo $row->installmentdate;?></td>
                                                        <td class=" "><?php echo $row->remarks;?></td>

                                                        <td class="center ">
                                                            <a class="tooltip-demo" href="<?php echo base_url('emp_csd_cont/UpdateCSD/'.$this->uri->segment(3).'/'.$row->id)?>"><li class="fa fa-edit" title="Edit CSD Record"></li></a>
                                                            <a class="tooltip-demo" href="<?php echo base_url('emp_csd_cont/DeleteCSD/'.$this->uri->segment(3).'/'.$row->id)?>"><li class="fa fa-trash-o" title="Delete CSD Record"></li></a>


                                                        </td>
                                                    </tr>
                                        <?php       $count++;
                                                endforeach;
                                            endif;?>
                                        
                                    </tbody>
                                </table>

                            </div>
                            <!-- /.table-responsive -->
                        </div>
                        <!-- /.portlet-body -->
                    </div>
                    <!-- /.portlet -->

                </div>
                <!-- /.col-lg-12 -->

            </div>
            <!-- /.row -->

        </div>
        <!-- /.page-content -->

    </div>
</div>