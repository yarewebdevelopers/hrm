<div id="page-wrapper">

<div class="page-content page-content-ease-in">

    <!-- begin PAGE TITLE ROW -->
    <div class="row">
        <div class="col-lg-12">
            <div class="page-title">
                <h1>Classes
                    <a href="<?php echo base_url('emp_emp_cont/addEmp');?>"><input class="btn btn-default" type="submit" value="Add Classes"/></a>
                    <a href="<?php echo base_url('emp_emp_cont/addEmp');?>"><input class="btn btn-danger" type="submit" value="Trash"/></a>
                </h1>
                <ol class="breadcrumb">
                    <li><i class="fa fa-dashboard"></i>  <a href="index.html">Staff Management</a>
                    </li>
                    <li class="active">Classes</li>
                </ol>
            </div>
        </div>
        <!-- /.col-lg-12 -->
    </div>
    <!-- /.row -->
    <!-- end PAGE TITLE ROW -->
    <div class="row">

            <!-- Validation States -->
            <div class="col-lg-12">
                <div class="portlet portlet-default">
                    <div class="portlet-heading">
                        <div class="portlet-title">
                            <h4>Employee Profile</h4>
                        </div>
                        <div class="clearfix">
                            
                        </div>
                    </div>
                    <div class="panel-collapse collapse in" id="validationExamples">
                        <div class="portlet-body">
                                
                                <!-- Start of father's name name and number fields -->
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="number">Number</label>
                                    <div class="col-sm-4">
                                        <span></span>
                                        
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="name">Rank/Name</label>
                                    <div class="col-sm-4">
                                        <span></span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="name">Office</label>
                                    <div class="col-sm-4">
                                        <span></span>
                                    </div>
                                    <div class="clearfix"></div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    <!-- begin ADVANCED TABLES ROW -->
    <div class="row">

        <div class="col-lg-12">

            <div class="portlet portlet-default">
                <div class="portlet-heading">
                    <div class="portlet-title">
                        <h4>Lacking Classes</h4>
                    </div>
                    <div class="clearfix"></div>
                </div>
                <div class="portlet-body">
                    <div class="table-responsive">
                        <div role="grid" class="dataTables_wrapper form-inline" id="example-table_wrapper">

                            <table class="table table-striped table-bordered table-hover table-green dataTable" id="example-table" aria-describedby="example-table_info">

                                <thead>
                                    <tr role="row">
                                        <th class="sorting_asc" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width:auto;" aria-sort="ascending" aria-label="Rendering engine: activate to sort column descending">
                                            Sr.#
                                        </th>
                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Browser: activate to sort column ascending">
                                            Class Name
                                        </th>
                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="Platform(s): activate to sort column ascending">
                                            Remarks
                                        </th>
                                        
                                        <th class="sorting" role="columnheader" tabindex="0" aria-controls="example-table" rowspan="1" colspan="1" style="width: auto;" aria-label="CSS grade: activate to sort column ascending">
                                            Actions
                                        </th>
                                    </tr>
                                </thead>

                                <tbody role="alert" aria-live="polite" aria-relevant="all">
                                    <tr class="gradeA odd">
                                        <td class="  sorting_1">1</td>
                                        <td class=" ">ABC</td>
                                        <td class=" ">16</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-search"></li>
                                            <a title="Edit" href="<?php echo base_url('welcome/awards_edit');?>"><li class="fa fa-edit"></li></a>
                                            <li class="fa fa-trash-o"></li>
                                            <li class="fa fa-paperclip"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA even">
                                        <td class="  sorting_1">2</td>

                                        <td class=" ">TUV</td>
                                        <td class="center ">10</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                            <li class="fa fa-paperclip"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA odd">
                                        <td class="  sorting_1">3</td>

                                        <td class=" ">XYZ</td>
                                        <td class="center ">150</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                            <li class="fa fa-paperclip"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA even">
                                        <td class="  sorting_1">4</td>

                                        <td class=" ">ABC</td>
                                        <td class="center ">13</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                            <li class="fa fa-paperclip"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA odd">
                                        <td class="  sorting_1">5</td>

                                        <td class=" ">TUV</td>
                                        <td class="center ">6</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                            <li class="fa fa-paperclip"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA even">
                                        <td class="  sorting_1">6</td>

                                        <td class=" ">XYZ</td>
                                        <td class="center ">120</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                            <li class="fa fa-paperclip"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA odd">
                                        <td class="  sorting_1">7</td>

                                        <td class=" ">ABC</td>
                                        <td class="center ">15</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                            <li class="fa fa-paperclip"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA even">
                                        <td class="  sorting_1">8</td>

                                        <td class=" ">TUV</td>
                                        <td class="center ">9</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                            <li class="fa fa-paperclip"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA odd">
                                        <td class="  sorting_1">9</td>

                                        <td class=" ">XYZ</td>
                                        <td class="center ">160</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                            <li class="fa fa-paperclip"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA even">
                                        <td class="  sorting_1">10</td>

                                        <td class=" ">ABC</td>
                                        <td class="center ">20</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                            <li class="fa fa-paperclip"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA even">
                                        <td class="  sorting_1">11</td>

                                        <td class=" ">TUV</td>
                                        <td class="center ">2</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                            <li class="fa fa-paperclip"></li>
                                        </td>
                                    </tr>
                                    <tr class="gradeA even">
                                        <td class="  sorting_1">12</td>

                                        <td class=" ">XYZ</td>
                                        <td class="center ">180</td>
                                        
                                        <td class="center ">
                                            <li class="fa fa-edit"></li>
                                            <li class="fa fa-search"></li>
                                            <li class="fa fa-trash-o"></li>
                                            <li class="fa fa-paperclip"></li>
                                        </td>
                                    </tr>
                            </tbody>
                        </table>

                    </div>
                    <!-- /.table-responsive -->
                </div>
                <!-- /.portlet-body -->
            </div>
            <!-- /.portlet -->

        </div>
        <!-- /.col-lg-12 -->

    </div>
    <!-- /.row -->

</div>
<!-- /.page-content -->

</div>
</div>
