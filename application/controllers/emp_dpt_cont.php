<?php

    class Emp_dpt_cont extends CI_Controller
    {
        function __construct()
        {
            parent::__construct();
        }
        
        function index() 
        {
            $data['main_page'] = "departments";
            $this->load->view('includes/page2',$data);            
        }
    }