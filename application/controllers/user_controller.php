<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of login
 *
 * @author ali00
 */
class user_controller extends CI_Controller
{
    function __construct()
    {
        parent::__construct();        
    }
    
    ///////////////index function////////////////
    function index()
    {
	//$this->load->view('login/login');
        
        if($this->session->userdata('logged_in')!=NULL)
        {
            redirect(base_url('user_controller/load_pages/dashboard'));
        }
        else
        {
            $data['main_page'] = "login";
            $this->load->view('includes/page',$data);
        }
    }
	///////////////////load login form//////////////////////////
	public function load_login_page(){
	$data['main_page'] = "login";
	//$data['error_message'] = "check login credentials";	
     $this->load->view('includes/page',$data);
	} 
	///////////////////////////load  pages///////////////////////
	public function load_pages($page){
	if($this->session->userdata('logged_in')!=null){
			$data['main_page']=$page;
			$this->load->view('includes/page2',$data);
	}
	else{
		redirect(base_url());
	}
									}
	///////////////////////////load deleted users page////////////////////////////////////
	public function load_deleted_user_page($page){
	if($this->session->userdata('logged_in')!=null){
	$data['main_page']=$page;
	$this->load->model('users_Model');
	$data['users_data']=$this->users_Model->show_users_deleted();
	$this->load->view('includes/page2',$data);
													}
													else{
													redirect(base_url());
													}
	
	}
	//////////////////////////////////////////////////////////////
	//////////////////////////load users_page//////////////////////////////////
	public function load_userview_page($page){
	if($this->session->userdata('logged_in')!=null){
	//echo "done";exit;
	$data['main_page']=$page;
	$this->load->model('users_Model');
	$data['users_data']=$this->users_Model->show_user();
	$this->load->view('includes/page2',$data);
	}
	else{
	redirect(base_url());
	}
	}
	//////////////////////////////////////////////////////////
	
	
	////////////////check_login/////////////
    
    function check_Login() 
    {//function starting
	
				//granting access on seesion base
					
						
						if($this->session->userdata('logged_in')!=null){
						redirect(base_url().'user_controller/load_pages/dashboard');
						
						
						}
						else{//id 11
				
        
        $this->form_validation->set_rules('email','Email','trim|required|valid_email');
        $this->form_validation->set_rules('password','Password','required|min_length[5]');
        if($this->form_validation->run()==FALSE)
        {
            $this->load_login_page();
		 // redirect(base_url());
        }
        else
        {//main else
			$this->load->model('users_model');
            $result = $this->users_model->check_login();
					if($result){
                                            redirect(base_url().'user_controller/load_pages/dashboard');
									//$this->load->view('dashboard/dashboard');
									//echo $this->session->userdata('user_email');
					
								}
								else{
										$data['main_page'] = "login";
										$data['error_message'] = "check login credentials";	
										$this->load->view('includes/page',$data);
								//$this->load_login_page();
									
								}
								
           
              
         }//main else
	} //id 22
           
        
    }//function ending
	/////////////////logout function///////////////////
    
    function logout()
    {
		$this->load->model('users_model');//doing flag 0 for log off
		$this->users_model->logout_flag();
		$this->session->sess_destroy();
		//echo $this->session->userdata('user_email');
		
		redirect(base_url());
       //$this->load_login_page();
    }
	
	//////////////////////create user///////////////////////////
	public function create_user(){// creating user bu super admin
	///session checking
//	echo $this->input->post('name');exit;
					if($this->session->userdata('logged_in')!=null){//id one
								
							//	$data['users_data']=$this->show_userdata();
						
								//var_dump($data);exit;
		$this->form_validation->set_rules('email','Email','trim|required|valid_email');
		$this->form_validation->set_rules('name','Name','trim|required');
        $this->form_validation->set_rules('password','Password','required|min_length[5]');
		$this->form_validation->set_rules('role','Role','trim|required');
		$this->form_validation->set_rules('office','Office','trim|required');
		if($this->form_validation->run()==false){
	
	    $data['main_page']="users_add";
	    $this->load->view('includes/page2',$data);
	  // $this->load_pages($data);
		
		
												}
		else{//id 222
	 
	
			$this->load->model('users_Model');
			$check=$this->users_Model->create_user();//calling function in model
			if($check==false){//id99
			$data['error']="user already exists by this email";
			$data['main_page']="users_add";
			$this->load->view('includes/page2',$data);
			
	
	
							}//id 99
			else{//id100
						
			redirect(base_url('user_controller/load_userview_page/users'));
																		
																		
			
			}//id100
			}//id 222
																}//id one
		else{//id two
				redirect(base_url());
	
			}//id two
	}//function ending
	///////////////////////delete_user////////////////////////////////
	public function delete_user($id){
	//echo $id;exit;
	$this->load->model('users_Model');
	$this->users_Model->delete_user($id);
	
	redirect(base_url('user_controller/load_userview_page/users'));
	//$data['main_page']="users";
	//$this->load_userview_page(main);
	
	}
	////////////////////////////show_user///////////////////////////////
	public function show_userdata(){
	$this->load->model('users_Model');
	$result=$this->users_Model->show_user();
	//print_r($result);exit;
	return $result;
	
	}
	//////////////load users editing page after validation errors////////
	public function load_update_page($id){
			//$this->load->model->('users_Model');
			$data['idno']=$id;
			$this->load->model('users_Model');
			$data['edit_data']=$this->users_Model->edit_user_data($id);
			$data['main_page']="users_edit";
			//$data['user_data']=	$this->show_userdata();
			$this->load->view('includes/page2',$data);
	}
	///////////////////load_superadmin_adit_info page/////////////
	////////////////load_edit_superadmin_page//////
	public function load_edit_superadmin_page($id){
			if($this->session->userdata('logged_in')!=null){
			$data['idno']=$id;
			$this->load->model('users_Model');
			$data['edit_data']=$this->users_Model->edit_user_data($id);
			$data['main_page']="edit_loggedin_admin";
			//$data['user_data']=	$this->show_userdata();
			$this->load->view('includes/page2',$data);
			}
			else{
			redirect(base_url());
			}
	}
	/////////////////////////////////update_superadmindata//////////////////////////////////////////////
	public function update_superadmin($id){
	if($this->session->userdata('logged_in')!=null){//if of session
	
	    $this->form_validation->set_rules('email','Email','trim|required|valid_email');
		$this->form_validation->set_rules('name','Name','trim|required');
        $this->form_validation->set_rules('password','Password','required|min_length[5]');
		//$this->form_validation->set_rules('role','Role','required');
			if($this->form_validation->run()==false){
			$this->load_edit_superadmin_page($id);
			}
			else{
	        
	$this->load->model('users_Model');
	$this->users_Model->update_superadmin($id);
	redirect(base_url('user_controller/load_userview_page/users'));
	}
	}//if of session
	else{
	redirect(base_url());
	}
	
	}//function ending
	//////////////////////////////////////////////////////////////////////////////
	/////////////////////////////update_user//////////////////////////////////////
	public function update_user($id){
	   // $this->form_validation->set_rules('email','Email','trim|required|valid_email');
		$this->form_validation->set_rules('name','Name','trim|required');
        $this->form_validation->set_rules('password','Password','required|min_length[5]');
		$this->form_validation->set_rules('role','Role','trim|required');
		$this->form_validation->set_rules('office','Office','trim|required');
			if($this->form_validation->run()==false){
			$this->load_update_page($id);
			}
			else{//else 12
			
	        
	$this->load->model('users_Model');
	$this->users_Model->update_user($id);
	//redirect(base_url('index.php/user_controller/create_user'));
	redirect(base_url('user_controller/load_userview_page/users'));

					
	}//else 12
	
	}
	
	//////////////////////////////////////////////////////////////////////
	////////////////////////////////enable deleted users
	public function enable_deleted_users($id){
	if($this->session->userdata('logged_in')!=null){
	$this->load->model('users_Model');
	$this->users_Model->enable_deleted_users($id);
	//load_deleted_user_page($page)
	redirect(base_url('user_controller/load_deleted_user_page/deleted_user'));
	}
	else{
	redirect(base_url());
	}
	
	}
	
}//class ending
