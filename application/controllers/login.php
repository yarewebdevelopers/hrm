<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of login
 *
 * @author ali00
 */
class login extends CI_Controller
{
    function __construct()
    {
        parent::__construct();
        $this->load->model('usr_model','',TRUE);
    }
    
    
    function index()
    {
        $data['main_page'] = "login";
        for ($i = 0; $i < func_num_args(); $i++) 
        {
             $data['error_login']=func_get_arg($i);
        }
        $this->load->view('includes/page', $data);
    }
    
    function doLogin() 
    {
        
        $this->form_validation->set_rules('email','Email','trim|required|valid_email');
        $this->form_validation->set_rules('password','Password','required');
        if($this->form_validation->run()==FALSE)
        {
            $this->index();
        }
        else
        {
            $result = $this->usr_model->index();
            
            if($result)
            {
                $session_array= array();
                foreach($result as $row)
                {
                    
                    $session_array= array(
                        'id' => $row->id,
                        'name' => $row->name
                    );
                    
                }
                $this->session->set_userdata('logged_in',$session_array);
                redirect('welcome');
            }
           else
           {
               $this->index(TRUE);
           }
        }
    }
    
    
    function logout()
    {
        $this->session->destroy();
        header("Location: ".base_url()."login");
    }
}
