<?php
class Welcome extends CI_Controller {

	public function index()
	{
            $data['main_page'] = "dashboard";
            $this->load->view('includes/page2',$data);
	}
        
        
        function departments()
        {
            $data['main_page'] = "departments";
            $this->load->view('includes/page2',$data);
        }
        function family()
        {
            $data['main_page'] = "family";
            $this->load->view('includes/page2',$data);
        }
        function family_add()
        {
            $data['main_page'] = "family_add";
            $this->load->view('includes/page2',$data);
        }
        function family_edit()
        {
            $data['main_page'] = "family_edit";
            $this->load->view('includes/page2',$data);
        }
        function caste()
        {
            $data['main_page'] = "caste";
            $this->load->view('includes/page2',$data);
        }
        function csd()
        {
            $data['main_page'] = "csd";
            $this->load->view('includes/page2',$data);
        }
        function csd_add()
        {
            $data['main_page'] = "csd_add";
            $this->load->view('includes/page2',$data);
        }
        function csd_add_emp()
        {
            $data['main_page'] = "csd_add_emp";
            $this->load->view('includes/page2',$data);
        }
        function csd_edit_emp()
        {
            $data['main_page'] = "csd_edit_emp";
            $this->load->view('includes/page2',$data);
        }
        function csd_emp()
        {
            $data['main_page'] = "csd_emp";
            $this->load->view('includes/page2',$data);
        }
        function awards()
        {
            $data['main_page'] = "awards";
            $this->load->view('includes/page2',$data);
        }
        function awards_emp()
        {
            $data['main_page'] = "awards_emp";
            $this->load->view('includes/page2',$data);
        }
        function awards_add()
        {
            $data['main_page'] = "awards_add";
            $this->load->view('includes/page2',$data);
        }
        function awards_add_emp()
        {
            $data['main_page'] = "awards_add_emp";
            $this->load->view('includes/page2',$data);
        }
        function awards_edit_emp()
        {
            $data['main_page'] = "awards_edit_emp";
            $this->load->view('includes/page2',$data);
        }
        function users()
        {
            $data['main_page'] = "users";
            $this->load->view('includes/page2',$data);
        }
        function users_add()
        {
            $data['main_page'] = "users_add";
            $this->load->view('includes/page2',$data);
        }
        function users_edit()
        {
            $data['main_page'] = "users_edit";
            $this->load->view('includes/page2',$data);
        }
        function employee_add()
        {
            $data['main_page'] = "employee_add";
            $this->load->view('includes/page2',$data);
        }
        function employee_view()
        {
            $data['main_page'] = "employee";
            $this->load->view('includes/page2',$data);
        }
        function employee_edit()
        {
            $data['main_page'] = "employee_edit";
            $this->load->view('includes/page2',$data);
        }
        function attachments()
        {
            $data['main_page'] = "attachments";
            $this->load->view('includes/page2',$data);
        }
        function attachments_add()
        {
            $data['main_page'] = "attachments_add";
            $this->load->view('includes/page2',$data);
        }
        function attachments_edit()
        {
            $data['main_page'] = "attachments_edit";
            $this->load->view('includes/page2',$data);
        }
        function installments()
        {
            $data['main_page'] = "installments";
            $this->load->view('includes/page2',$data);
        }
        
        function inquiry()
        {
            $data['main_page'] = "inquires";
            $this->load->view('includes/page2',$data);
        }
        function leaves()
        {
            $data['main_page'] = "leaves";
            $this->load->view('includes/page2',$data);
        }
        function leaves_add_emp()
        {
            $data['main_page'] = "leaves_add_emp";
            $this->load->view('includes/page2',$data);
        }
        function leaves_edit_emp()
        {
            $data['main_page'] = "leaves_edit_emp";
            $this->load->view('includes/page2',$data);
        }
        function leaves_emp()
        {
            $data['main_page'] = "leaves_emp";
            $this->load->view('includes/page2',$data);
        }
        function promotions()
        {
            $data['main_page'] = "promotions";
            $this->load->view('includes/page2',$data);
        }
        function uniforms()
        {
            $data['main_page'] = "uniforms";
            $this->load->view('includes/page2',$data);
        }
        function classes()
        {
            $data['main_page'] = "classes";
            $this->load->view('includes/page2',$data);
        }
        function logout()
        {
            $this->session->unset_userdata("logged_in");
            $data['main_page'] = "login";
            $this->load->view('includes/page',$data);
        }
        
        
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */