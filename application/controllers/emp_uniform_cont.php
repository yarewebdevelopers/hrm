<?php
class emp_uniform_cont extends CI_Controller{
	function __construct(){
		parent::__construct();
	}
	function index(){
            $data['main_page'] = "uniforms";
            $this->load->view('includes/page2',$data);
	
	}
	function getURecord(){
		$data['records']=$this->emp_model->tableGet('uniformrecord');
		print_r($data);
                $data['main_page'] = "uniforms";
                $this->load->view('includes/page2',$data);
		//$this->load->view('uniform',$data);
	
	}
	function getURecordById(){
		$data['records']=$this->emp_model->tableGetById('uniformrecord');
		print_r($data);
                $data['main_page'] = "uniforms";
                $this->load->view('includes/page2',$data);
		
//$this->load->view('uniform',$data);
	
	}
	function addURecord(){
            if($this->input->post('submit')){
		$data=array(
		'rank'=>$this->input->post('rank'),
		'uniformno'=>$this->input->post('uniformno'),
		'bdprocdate'=>$this->input->post('bdprocdate'),
		'remarks'=>$this->input->post('remarks'),
		'eid'=>$this->input->post('eid'),
		'name'=>$this->input->post('name')
		
		);
		if($this->emp_model->addRecord('uniformrecord')){
			$data['msg']="Record Added Successfully";
			//$this->load->view('uniform',$data);
		
		}
		else{
			$data['msg']="Failed";
			//$this->load->view('uniform',$data);
		
		}
            }else
            {
                $data['main_page'] = "uniforms_add";
                $this->load->view('includes/page2',$data);
            }
            
	
	}
	function updateURecord(){
            if($this->input->post('submit'))
            {
			$data=array(
		'rank'=>$this->input->post('rank'),
		'uniformno'=>$this->input->post('uniformno'),
		'bdprocdate'=>$this->input->post('bdprocdate'),
		'remarks'=>$this->input->post('remarks'),
		'eid'=>$this->input->post('eid'),
		'name'=>$this->input->post('name')
		
		);
		if($this->emp_model->updateTableRecord('uniformrecord')){
			$data['msg']="Record Updated Successfully";
			//$this->load->view('uniform',$data);
		
		}
		else{
			$data['msg']="Failed";
			//$this->load->view('uniform',$data);
		
		}
            }
            else
            {
                $data['main_page'] = "uniforms_edit";
                $this->load->view('includes/page2',$data);
            }
	
	}
	function deleteURecord(){
		if($this->emp_model->deleteTableRecord('uniformrecord')){
			$data['msg']="Record Deleted Successfully";
			//$this->load->view('uniform',$data);
		
		
		
		
		}
		else{
			$data['msg']="Failed";
			//$this->load->view('uniform',$data);
		
		
		}
	
	
	}

}