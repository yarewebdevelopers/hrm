<?php
class emp_leave_cont extends CI_Controller{
	function __construct(){
            parent::__construct();
	}
	function index(){
		
		//$this->getLeaveRecordById();
		
	}
	function getLeaveRecord(){
		$data['records']=$this->emp_model->tableGet('leaverecord');
		$data['main_page'] = "leaves_emp";
            $this->load->view('includes/page2',$data);
		//$this->load->view('leaverecord',$data);
		
	
	}
	function getLeaveRecordById(){
		$data['main_page']='leaves_emp';
		$data['records']=$this->emp_model->tableGetById('leaverecord');
	    $data['empid']=$this->uri->segment(3);
		//print_r($data);exit;
	    $this->load->view('includes/page2',$data);
		
	}
	function addLeave(){
		
		if($this->input->post('submit'))
        {
                
         $data=array(
		'from'=>$this->input->post('from'),
		'to'=>$this->input->post('to'),
		'reason'=>$this->input->post('reason'),
		'remarks'=>$this->input->post('remarks'),
		'eid'=>$this->input->post('eid'),
                'status'=>1
		
		);
		//print_r($data);exit;
		if($this->emp_model->addRecord('leaverecord',$data)){
			
			redirect('emp_leave_cont/addLeave/'.$this->input->post('eid').'/success');
			
			
		}
		else{
			$data['msg']="Failed";
			//$this->load->view('leave',$data);
		}
                    }
                else{
                    $data['main_page'] = "leaves_add_emp";
                    $data['records']=$this->emp_model->tableGetEmpById();
                    $this->load->view('includes/page2',$data);
                }
	}
	function updateLeave(){
         if ($this->input->post('submit')) {
              
            $data=array(
		'from'=>$this->input->post('from'),
		'to'=>$this->input->post('to'),
		'reason'=>$this->input->post('reason'),
		'remarks'=>$this->input->post('remarks')
		
		);
		//print_r($data);exit;
		if($this->emp_model->updateTableRecord('leaverecord',$data)){
			//$data['msg']="Record Updated Successfully";
			
			
			redirect('emp_leave_cont/updateLeave/'.$this->input->post('eid').'/'.$this->uri->segment(3).'/success');
			
			
		
		    
		}
		else{
			$data['msg']="Failed";
			//$this->load->view('leave',$data);
		}
            }
            else{
                    $data['main_page'] = "leaves_edit_emp";
                $data['records']=$this->emp_model->tableGetById('leaverecord');
                $data['employee']=$this->emp_model->tableGetEmpById();
                $this->load->view('includes/page2',$data);
                }
	
	}
	function deleteLeave(){
		if($this->emp_model->deleteTableRecord('leaverecord')){
			//echo "success";exit;
	    $data['msg']="Record Deleted Successfully";	
	    $data['main_page']='leaves_emp';
		$data['records']=$this->emp_model->tableGetById('leaverecord');
	    $data['empid']=$this->uri->segment(3);
		//print_r($data);exit;
	    $this->load->view('includes/page2',$data);
		//	$this->load->view('leave',$data);
		
		}
		else{
			$data['msg']="Failed";
                        echo 'failed';
			//$this->load->view('leave',$data);
		
		}
	}
	
}