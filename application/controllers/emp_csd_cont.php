<?php
class emp_csd_cont extends CI_Controller{

	function __construct(){
		parent::__construct();
	
	
	}
	function index(){
            $data['main_page'] = "csd";
            $this->load->view('includes/page2',$data);
	}
	function getCsd(){
		$data['records']=$this->emp_model->tableGet('csdprocurement');
		print_r($data);exit;
		//$this->load->view('csd',$data);
		
	
	
	}
	function getCsdById(){
		$data['records']=$this->emp_model->tableGetById('csdprocurement');
                $data['main_page'] = "csd_emp";
                $this->load->view('includes/page2',$data);
		//$this->load->view('csd',$data);
		
		
	}
	function addCsd($id)
        {
            
            if(!$this->input->post('submit'))
            {
            
                $data['main_page'] = "csd_add_emp";
                $data['records'] = $this->emp_model->tablegetEmpById();
                $data['rank'] = $this->emp_model->tableGet('rank');
                $data['cast'] = $this->emp_model->tableGet('cast');
            
                $this->load->view('includes/page2',$data);
            }
            else
            {
		$data=array(
		'items'=>$this->input->post('items'),
		'installmentspaid'=>$this->input->post('installmentspaid'),
		'installmentspending'=>$this->input->post('installmentspending'),
		'installmentdate'=>$this->input->post('installmentsdelayed'),
		'installmentsdelayed'=>$this->input->post('installmentsdelayed'),
		'remarks'=>$this->input->post('remarks'),
		'eid'=>$id,
		'status'=>1,
		);
		if($this->emp_model->addRecord('csdprocurement',$data))
                {
		
			$data['msg']="Record Added Successfully";
			redirect(base_url('emp_csd_cont/getCSDById/'.$id));
		}
		else
                {
			$data['msg']="Failed";
			print_r($data);exit;
			//$this->load->view('csd',$data);
                }
            
            }
	}
	
        function updateCsd(){
		if(!$this->input->post('submit'))
            {
            
                $data['main_page'] = "csd_edit_emp";
                $data['records'] = $this->emp_model->tablegetEmpById($this->uri->segment(3));
                $data['rank'] = $this->emp_model->tableGet('rank');
                $data['cast'] = $this->emp_model->tableGet('cast');
                $data['csdProcurement'] = $this->emp_model->tableGetbyId('csdProcurement',$this->uri->segment(4));
                
              
                
                $this->load->view('includes/page2',$data);
            }
            else
            {
                $data=array(
		'namerank'=>$this->input->post('namerank'),
		'items'=>$this->input->post('items'),
		'installmentspaid'=>$this->input->post('installmentspaid'),
		'installmentspending'=>$this->input->post('installmentspending'),
		'installmentdate'=>$this->input->post('installmentsdelayed'),
		'installmentsdelayed'=>$this->input->post('installmentsdelayed'),
		'remarks'=>$this->input->post('remarks')
		);
		if($this->emp_model->updateTableRecord('csdprocurement',$data)){
		
			$data['msg']="Record Update Successfully";
			redirect(base_url('emp_emp_cont'));
		
		
		}
		else{
			$data['msg']="Failed";
			print_r($data);exit;
			//$this->load->view('csd',$data);

		}
            }
        }
		
		
	
	function deleteCsd(){
                $data=array('status'=>0);
		if($this->emp_model->deleteTableRecord('csdprocurement'))
		{
			$data['msg']="Record Deleted Successfully";
			print_r($data);
			redirect(base_url('emp_csd_cont/getCsdById/'.$this->uri->segment(3)));
		
		}
		else{
			$data['msg']="Failed";
			//$this->load->view('csd',$data);
		
		}
	
	
	}


}