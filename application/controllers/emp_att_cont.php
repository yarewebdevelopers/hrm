<?php
class emp_att_cont extends CI_Controller{

	function __construct()
    {

		parent::__construct();


                           }
 function index()
 {
    $data['main_page'] = "attachments";
    $this->load->view('includes/page2',$data);
 }
 function getAtt(){
        $data['records']=$this->emp_model->tableGet('attachment');   
        $data['main_page'] = "attachments";
        $this->load->view('includes/page2',$data);
 	
 	print_r($data);exit;
 	//$this->load->view('attachment',$data);
 	
 
 
 }
 function getAttById($id){
 	$data['records']=$this->emp_model->tableGetById('attachment',$id);
 	$data['main_page'] = "attachments_emp";
 	
     $this->load->view('includes/page2',$data);
 
 
 }
 function getAttP(){
 	$data['records']=$this->emp_model->getAttP($this->uri->segment(4));
 	$data['employee']=$this->emp_model->tableGetEmpById();
    
 	$data['main_page'] = "attachments_p";
     $this->load->view('includes/page2',$data);
 	
 	
 	
    
 
 }
 function addAtt($id){
        if(!$this->input->post('submit'))
        {
            
            $data['main_page'] = "attachments_add";
            $data['records'] = $this->emp_model->tablegetEmpById();
  
            
            $this->load->view('includes/page2',$data);
            
        }
        else
        {
            
            $data=array(
            'attwith'=>$this->input->post('attwith'),
            'from'=>$this->input->post('from'),
            'to'=>$this->input->post('to'),
            'reldate'=>$this->input->post('reldate'),
            'runningdays'=>$this->input->post('runningdays'),
            'remarks'=>$this->input->post('remarks'),
            'eid'=>$id
            );
            if($this->emp_model->addRecord('attachment',$data)){
                    $data['msg']="Record added Successfully";
                    redirect(base_url('emp_att_cont/getAttById/'.$id));

            }
            else{
                    $data['msg']="Failed";
                    //$this->load->view('attachment',$data);
                    redirect(base_url('emp_att_cont/getAttById/'.$id));
            }
        }
 }
 function updateAtt(){
            if(!$this->input->post('submit'))
        {
            
            $data['main_page'] = "attachments_edit";
            $data['employee'] = $this->emp_model->tablegetEmpById();
            //echo $this->uri->segment(4);exit;
            $data['records']=$this->emp_model->getAttP($this->uri->segment(4));
            
            
            $this->load->view('includes/page2',$data);
            
        }
 else {

        $data=array(
               
               'attwith'=>$this->input->post('attwith'),
               'to'=>$this->input->post('to'),
               'from'=>$this->input->post('from'),
               'reldate'=>$this->input->post('reldate'),
               'runningdays'=>$this->input->post('runningdays'),
               'remarks'=>$this->input->post('remarks')

               );
             //  print_r($data);exit;
               if($this->emp_model->updateTableRecord('attachment',$data)){
                       $data['msg']="Record Updated Successfully";
                       //echo $this->input->post('eid');exit;
                       $url='emp_att_cont/updateAtt/'.$this->input->post('eid').'/'.$this->input->post('id').'/success';
                       
                      redirect($url);

               }
               else{
                       $data['msg']="Failed";

               }
               
        }
 }
 
 function deleteAtt(){
 	if($this->emp_model->deleteTableRecord('attachment')){
 		$this->getAttById($this->uri->segment(4));
 		
 	
 	
 	}
 	else{
 
 
 	
 	
 	}
 	
 
 
 }
function load_deleted_att($id){
	//echo $id;exit;
	//print_r($this->emp_model->getDeletedRecords($id,"attachment"));exit;
	$data['records']=$this->emp_model->getDeletedRecords($id,"attachment");
	$data['main_page']='attachments_emp';
	$data['trash']=true;
	$this->load->view('includes/page2',$data);
	
	




}
function recover($id){
$data = array('status' => 1);
            if($this->emp_model->updateAtt('attachment',$data))
            {
                
                $data['msg']="Record Recovered Successfully";
               // redirect(base_url('emp_emp_cont'));
               
               
            }
	
            else
            {    
                
		$data['msg']="Failed";
	//	redirect(base_url('emp_emp_cont'));
            }
         $data['main_page']='attachments_emp';
         $data['trash']=true;
		 $data['records']=$this->emp_model->getDeletedRecords($this->uri->segment(3),'attachment');
		 $this->load->view('includes/page2',$data);
	
	



}


}