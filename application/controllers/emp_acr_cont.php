<?php
class emp_acr_cont extends CI_Controller{
	function __construct(){
		
	parent::__construct();
	}
	
	
	function index(){
		
	
	}
	function getAcr(){
		$data['records']=$this->emp_model->tableGet('acr');
		print_r($data);
                $data['main_page'] = "acr";
                $this->load->view('includes/page2',$data);
                
	
	}
	function getAcrById(){
		$data['records']=$this->emp_model->tableGetById('acr');
		print_r($data);
                $data['main_page'] = "acr";
                $this->load->view('includes/page2',$data);
		//$this->load->view('acr',$data);
	
	
	}
	function addAcr(){
            if($this->input->post('submit')){	$data=array(
		'namerank'=>$this->input->post('namerank'),
		'grading'=>$this->input->post('grading'),
		'sroremarks'=>$this->input->post('sroremarks'),
		'nsroremarks'=>$this->input->post('nsroremarks'),
		'nextrank'=>$this->input->post('nextrank'),
		'remarks'=>$this->input->post('remarks'),
		'year'=>$this->input->post('year'),
		'eid'=>$this->input->post('eid')
		);
		if($this->emp_model->addRecord('acr',$data)){
		
			$data['msg']="Record Added Successfully";
			//$this->load->view('acr',$data);
		
		
		}
		else{
			$data['msg']="Failed";
			//$this->load->view('acr',$data);
		
		}
            }
            else
            {
                $data['main_page'] = "acr_add";
                $this->load->view('includes/page2',$data);
            }
	
	
	}
	function updateAcr(){
            if($this->input->post('submit'))
            {
		$data=array(
		'namerank'=>$this->input->post('namerank'),
		'grading'=>$this->input->post('grading'),
		'sroremarks'=>$this->input->post('sroremarks'),
		'nsroremarks'=>$this->input->post('nsroremarks'),
		'nextrank'=>$this->input->post('nextrank'),
		'remarks'=>$this->input->post('remarks'),
		'year'=>$this->input->post('year'),
		'eid'=>$this->input->post('eid')
		);
		if($this->emp_model->updateTableRecord('acr',$data)){
		
			$data['msg']="Record Updated Successfully";
			//$this->load->view('acr',$data);
		
		}
		else{
			$data['msg']="Failed";
			//$this->load->view('acr',$data);
		}
            }
            else
            {
                $data['main_page'] = "acr_edit";
                $this->load->view('includes/page2',$data);
            }
	
	}
	function deleteAcr(){
		if($this->emp_model->deleteTableRecord('acr')){
		
		$data['msg']="Record Deleted Successfully";
		$this->getAcrById();
		}
		else{
			$data['msg']="Failed";
			$this->getAcrById();
                }
	}

}