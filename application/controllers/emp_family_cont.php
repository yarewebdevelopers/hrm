<?php
class emp_family_cont extends CI_Controller{
	function __construct(){
	parent::__construct();
	
	}
	function index(){
            
	}
	function get_emp_family_record($id){
		
		$this->load->model('emp_family_model');
		$data['records']=$this->emp_family_model->get_emp_family_record($id);
		$data['person_record']=$this->emp_family_model->get_emp_personal_record($id);
		//print_r($data['records']);exit;
                $data['main_page'] = "family";
               // $data['cities']=$this->emp_model->getCities();
                $this->load->view('includes/page2',$data);

		
	
	
	
	}
	function getEmpFmlByID(){
                
		$data['records']=$this->emp_model->tableGetById('family');
			print_r($data);exit;
	//	$this->load->view('emp',$data);
		
	
	}
	
	
	function load_pages($page){
	$data['main_page']=$page;
	$this->load->view('includes/page2',$data);
	
	}
	
	function load_add_emp_family_page($id){
	
	$this->load->model('emp_family_model');
	
	$data['person_record']=$this->emp_family_model->get_emp_personal_record($id);
	$data['cities']=$this->emp_model->getCities();
	$data['main_page'] = "family_add";
	
     $this->load->view('includes/page2',$data);
				}
		
	
	
	function add_emp_family_record($id){

	$this->form_validation->set_rules('name','Name','trim|required');
	$this->form_validation->set_rules('relation','Relation','trim|required');
	$this->form_validation->set_rules('age','Age','trim|required');
	$this->form_validation->set_rules('status','Status','trim|required');
	$this->form_validation->set_rules('address','Address','trim|required');
	$this->form_validation->set_rules('city','City','trim|required');
	$this->form_validation->set_rules('profession','Profession','trim|required');
//	$this->form_validation->set_rules('distr','distr','trim|requied');
	$this->form_validation->set_rules('code','Code','trim|requied');
	//$this->form_validation->set_rules('p_number','p_number','trim|requied');
	if($this->form_validation->run()==false)
	{
			$this->load_add_emp_family_page($id);
	}
	else{
		$this->load->model('emp_family_model');
		$this->emp_family_model->add_emp_family_record($id);
		redirect(base_url('emp_family_cont/get_emp_family_record/'.$id));
	}
	
	
	}
	
	function load_update_emp_family_record_page($id){
	$this->load->model('emp_family_model');
		$data['records']=$this->emp_family_model->get_emp_fam_record_for_edit($id);
		//$data['person_record']=$this->emp_family_model->get_emp_personal_record_for_edit_page($id);
		//print_r($data['records']);exit;
		//print_r($data);exit;
                $data['main_page'] = "family_edit";
                $data['cities']=$this->emp_model->getCities();
                $this->load->view('includes/page2',$data);
	
	}
	
	
	function update_emp_family_record($id){
	$this->form_validation->set_rules('name','Name','trim|required');
	$this->form_validation->set_rules('relation','Relation','trim|required');
	$this->form_validation->set_rules('age','Age','trim|required');
	$this->form_validation->set_rules('status','Status','trim|required');
	$this->form_validation->set_rules('address','Address','trim|required');
	$this->form_validation->set_rules('city','City','trim|required');
	$this->form_validation->set_rules('profession','Profession','trim|required');
//	$this->form_validation->set_rules('distr','distr','trim|requied');
	$this->form_validation->set_rules('code','Code','trim|requied');
	//$this->form_validation->set_rules('p_number','p_number','trim|requied');
	if($this->form_validation->run()==false)
	{
			$this->load_update_emp_family_record_page($id);
	}
	else{
		$this->load->model('emp_family_model');
		$data=$this->emp_family_model->update_emp_family_record($id);
		redirect(base_url('emp_family_cont/get_emp_family_record/'.$data));
	}
	}

	
	
	function load_deleted_family_page($id){
	
		$this->load->model('emp_family_model');
		$data['records']=$this->emp_family_model->get_emp_fam_record_deleted($id);
		$data['person_record']=	$this->emp_family_model->get_emp_detail($id);
         $data['main_page'] = "deleted_family";
         $this->load->view('includes/page2',$data);
	
	}
	
	function delete_emp_family_record($id){
		$this->load->model('emp_family_model');
		$data=$this->emp_family_model->del_emp_fam_record($id);
		//echo $data;exit;
		redirect(base_url('emp_family_cont/get_emp_family_record/'.$data));
		
	
	}
	function recover_deleted_record($id){
	$this->load->model('emp_family_model');
	$data=$this->emp_family_model->recover_deleted_record($id);
	//redirect(base_url('emp_family_cont/load_deleted_family_page/'.$data));
	$this->load_deleted_family_page($data);
	
	
	}
	
	
	



}