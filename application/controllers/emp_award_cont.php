<?php

session_start();
class emp_award_cont extends CI_Controller{
	function __construct(){
	parent::__construct();
	
	}
	function index(){
            $data['main_page'] = "awards_emp";
            $this->load->view('includes/page2',$data);
	
	
	}
	function getEmpAward(){
	$data['records']=$this->emp_model->tableGet('awards');
	print_r($data);exit;
	//$this->load->view('award',$data);
	
	
	}
	function getEmpAwardById(){
		$data['records']=$this->emp_model->tableGetEmpById();
		$data['awardss']=$this->emp_model->tableGetById('awards');
                $data['main_page'] = "awards_emp";
		$this->load->view('includes/page2',$data);
	
	
	}
	function addEmpAward(){
            if($this->input->post('submit')){
            
                $data=array(
		
		'award'=>$this->input->post('award'),
		'remarks'=>$this->input->post('remarks'),
		'eid'=>$this->uri->segment(3)
		
		);
		if($this->emp_model->addRecord('awards',$data)){
			
		
                        $_SESSION['msg']="Record Added Successfully";
                        $_SESSION['success'] = TRUE;
                        $_SESSION['failed'] = FALSE;
			redirect(base_url('emp_award_cont/getEmpAwardById/'.$this->uri->segment(3)));//$this->load->view('awards',$data);
		
		
		
		}
		else{
			$_SESSION['msg']="Failed To Add Record";
			$_SESSION['success_add'] = FALSE;
                        $_SESSION['failed_add'] = TRUE;
                        redirect(base_url('emp_award_cont/getEmpAwardById/'.$this->uri->segment(3)));
		
		}
            }
            else
            {
                $data['records']=$this->emp_model->tableGetEmpById();
                $data['main_page'] = "awards_add_emp";
		$this->load->view('includes/page2',$data);
            }
        }

	function updateEmpAward(){
            if($this->input->post('submit')){
		$data=array(
		'award'=>$this->input->post('award'),
		'remarks'=>$this->input->post('remarks')
		
		);
		if($this->emp_model->updateTableRecord('awards',$data)){
			
		$_SESSION['msg']="Record Updated Successfully";
                        $_SESSION['success']=TRUE;
                        $_SESSION['failure']=FALSE;
                        redirect(base_url('emp_award_cont/getEmpAwardById/'.$this->uri->segment(3)));
		//$this->load->view('awards',$data);
		
		
		
		}
		else{
			$_SESSION['msg']="Failed To Update Record";
                        $_SESSION['success']=FALSE;
                        $_SESSION['failure']=TRUE;
                        redirect(base_url('emp_award_cont/getEmpAwardById/'.$this->uri->segment(3)));
			//$this->load->view('awards',$data);
		
		}
            }
            else
            {
                $data['records']=$this->emp_model->tableGetEmpById();
                $data['awardss']=$this->emp_model->tableGetById('awards');
                $data['main_page'] = "awards_edit_emp";
		$this->load->view('includes/page2',$data);
            }
		
	
	
	}
        
        function load_deleted_awards(){
    	$data['records']=$this->emp_model->tableGetEmpById();
            $data['awardss']=$this->emp_model->getdeleted('awards');
            $data['main_page'] = "awards_emp";
            $data['trash']=TRUE;
            $this->load->view('includes/page2',$data);
    	}
        
	function deleteEmpAwards(){
	
		if($this->emp_model->deleteTableRecord('awards')){
			
                        $_SESSION['success']=TRUE;
                        $_SESSION['failure']=FALSE;
                        $_SESSION['msg']="Record Deleted Successfully";
                        redirect(base_url('emp_award_cont/getEmpAwardById/'.$this->uri->segment(3)));
			//$this->load->view('awards',$data);
		}
		else{
			$_SESSION['success_del']=FALSE;
                        $_SESSION['failure_del']=TRUE;
			$_SESSION['msg']="Failed To Delete Record";
                        redirect(base_url('emp_award_cont/getEmpAwardById/'.$this->uri->segment(3)));
			//$this->load->view('awards,$data);
		}
	
	
	}

        
        
        function recover(){
        $data=array('status'=>1);
    	if($this->emp_model->updateTableRecord('awards',$data))
        {
            $_SESSION['success']=TRUE;
            $_SESSION['failure']=FALSE;
            $_SESSION['msg']="Record Recovered Successfully";
            redirect(base_url('emp_award_cont/load_deleted_awards/'.$this->uri->segment(3)));
        }
        else
        {
            $_SESSION['success']=FALSE;
            $_SESSION['failure']=TRUE;
            $_SESSION['msg']="Failed To Recover Record";
            redirect(base_url('emp_award_cont/load_deleted_awards/'.$this->uri->segment(3)));
        }
    
    
    
    }
}