<?php
class emp_emp_cont extends CI_Controller
{
	function __construct(){
	
	parent::__construct();
	
	}
	
	function index(){
		$data['main_page']='employee';
		$data['records']=$this->emp_model->tableGetEmp();
		$this->load->view('includes/page2',$data);
		
	}
	function getEmp(){
		$data['records']=$this->emp_model->tableGetEmp();
		print_r($data);exit;
		//$this->load->view('employe',$data);
	}
	function getEmpById(){
		
		$data['records']=$this->emp_model->tableGetEmpById();
		//print_r($data);exit;
		$data['main_page']='employee_profile';
	    $this->load->view('includes/page2',$data);
	
	
	}
	function AddEmp(){
	
		
		if(!$this->input->post('no')){
	        $data['main_page']='employee_add';
	        $data['ranks']=$this->emp_model->tableGet('rank');
	        $data['casts']=$this->emp_model->tableGet('cast');
	        $data['cities']=$this->emp_model->getCities();
	        $this->load->view('includes/page2',$data);
			
			
		
		
		}
		else{
		$data=array(
		'no'=>$this->input->post('no'),
		'rankid'=>$this->input->post('rankid'),
		'name'=>$this->input->post('name'),
		'doj'=>$this->input->post('doj'),
		'dob'=>$this->input->post('dob'),
		'cnicno'=>$this->input->post('cnicno'),
		'cellno'=>$this->input->post('phone_code').$this->input->post('p_number'),
		'fathername'=>$this->input->post('fathername'),
		'address'=>$this->input->post('address1'),
		'castid'=>$this->input->post('castid'),
		'bloodgroup'=>$this->input->post('bloodgroup'),
		'depid'=>'1',
		'dor'=>$this->input->post('dor'),
		'imageurl'=>'not setup',
		'repto'=>$this->input->post('repto'),
		'city'=>$this->input->post('city')
		);
		//print_r($data);exit;
	
		$this->uploadimage();
                
                
		if($this->emp_model->addRecord('employee',$data)){
			$data['msg']="Employee Added Successfully";
			$data['main_page']='employee_add';
	        $data['ranks']=$this->emp_model->tableGet('rank');
	        $data['casts']=$this->emp_model->tableGet('cast');
	        $this->load->view('includes/page2',$data);
	
		
		}
		else{
			$data['msg']="Failed";
			//$this->load->view('emp',$data);
		}
		}
	
	
	}
	function uploadimage(){
            
		$config['upload_path'] = './uploads/';
                $config['allowed_type']        = "gif|jpg|jpeg|png";
                $config['max_size']             = 0;
                $config['max_width']            = 0;
                $config['max_height']           = 0;
                $config['remove_spaces']        = TRUE;
		echo UPLOAD_PATH;
                exit();
                //echo "success";exit;
                if( ! is_dir(UPLOAD_PATH.'uploads') ){mkdir(UPLOAD_PATH.'uploads',0777,TRUE); };
                                $this->load->library('upload',$config);
				$this->upload->initialize($config);
                                
                              
				if($this->upload->do_upload('userfile'))
				{
                                     
					$data = array('upload_data' => $this->upload->data());
					print_r($data);exit;
					$this->load->view('upload_success',$data);
				}
				else
				{
                                    echo 'failed';
                                     print_r($this->upload->data());
                                exit;
				$error = array('error' => $this->upload->display_errors());
				print_r($error);exit;
				$this->load->view('file_view', $error);
				}    
	}
	function updateEmp(){
		
            
		if(!$this->input->post('no')){
		  $data['main_page']='employee_edit';
		  $data['records']=$this->emp_model->tableGetEmpById();
		  $data['ranks']=$this->emp_model->tableGet('rank');
		  $data['cities']=$this->emp_model->getCities();
	      
		  $data['casts']=$this->emp_model->tableGet('cast');
			
		
	      $this->load->view('includes/page2',$data);
		
		}
		else{
             //echo "reached";exit;
		$data=array(
		'no'=>$this->input->post('no'),
		'rankid'=>$this->input->post('rankid'),
		'name'=>$this->input->post('name'),
		'doj'=>$this->input->post('doj'),
		'dob'=>$this->input->post('dob'),
		'cnicno'=>$this->input->post('cnicno'),
	    'cellno'=>$this->input->post('phone_code').$this->input->post('p_number'),
		'fathername'=>$this->input->post('fathername'),
		'address'=>$this->input->post('address'),
		'castid'=>$this->input->post('castid'),
		'bloodgroup'=>$this->input->post('bloodgroup'),
		'city'=>$this->input->post('city'),
		'depid'=>$this->input->post('depid'),
		'status'=>'1'
		);
		//print_r($data);exit;
		if($this->emp_model->updateTableRecord('employee',$data)){
			//echo "success";exit;
		  $data['msg']="Employee Updated Successfully";
		  $data['main_page']='employee_edit';
		  $data['records']=$this->emp_model->tableGetEmpById();
		  $data['ranks']=$this->emp_model->tableGet('rank');
		  $data['cities']=$this->emp_model->getCities();
	      
		  $data['casts']=$this->emp_model->tableGet('cast');
		  redirect('emp_emp_cont/updateEmp/'.$this->input->post('eid').'/success');
			
		
	     // $this->load->view('includes/page2',$data);
		
		}
		else{
			$data['msg']="Failed";
			
		}
		}
	
	
	}
    function load_deleted_emp(){
    	$data['records']=$this->emp_model->getDeletedEmp();
    	$data['main_page']='employee';
    	$data['trash']=true;
    	$this->load->view('includes/page2',$data);
    	
    
    
    
    }
	function deleteEmp($id)
        {
            $data = array('status' => 0);
            if($this->emp_model->updateTableRecord('employee',$data))
            {
                
                $data['msg']="Record Deleted Successfully";
               // redirect(base_url('emp_emp_cont'));
               
               
            }
	
            else
            {    
                
		$data['msg']="Failed";
	//	redirect(base_url('emp_emp_cont'));
            }
         $data['main_page']='employee';
		$data['records']=$this->emp_model->tableGetEmp();
		$this->load->view('includes/page2',$data);

	}
	function recover($id){
		$data = array('status' => 1);
            if($this->emp_model->updateTableRecord('employee',$data))
            {
                
                $data['msg']="Record Recovered Successfully";
               // redirect(base_url('emp_emp_cont'));
               
               
            }
	
            else
            {    
                
		$data['msg']="Failed";
	//	redirect(base_url('emp_emp_cont'));
            }
         $data['main_page']='employee';
         $data['trash']=true;
		$data['records']=$this->emp_model->getDeletedEmp();
		$this->load->view('includes/page2',$data);
		
	
	
	
	}
}