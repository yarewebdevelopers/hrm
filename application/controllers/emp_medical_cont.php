<?php
session_start();
class emp_medical_cont extends CI_Controller{


	function __construct(){
		parent::__construct();
	
	}
	function index(){
	}
	function getEmpMedical(){
		$data['records']=$this->emp_model->tableGetEmpById();
		$data['meds']=$this->emp_model->tableGet('medicalrecord');
		$data['main_page'] = "medical";
		$this->load->view('includes/page2',$data);
	
	
	
	}
	function getEmpMedicalById(){
		$data['records']=$this->emp_model->tableGetEmpById();
		$data['meds']=$this->emp_model->tableGetById('medicalrecord');
                $data['main_page'] = "medical";
		$this->load->view('includes/page2',$data);		
//$this->load->view('medical',$data);
	
	
	
	}
	function addMedical(){
            if($this->input->post('submit')){
            $data=array(
		'cat'=>$this->input->post('cat'),
		'admittedcmh'=>$this->input->post('cmh'),
		'from'=>$this->input->post('from'),
		'to'=>$this->input->post('to'),
		'partiio'=>$this->input->post('partiio'),
		'diseases'=>$this->input->post('diseases'),
		'disposal'=>$this->input->post('disposal'),
		'remarks'=>$this->input->post('remarks'),
		'review'=>$this->input->post('review'),
		'eid'=>$this->uri->segment(3)
		
		);
		if($this->emp_model->addRecord('medicalrecord',$data)){
			$_SESSION['msg']="Record Added Successfully";
                        $_SESSION['success'] = TRUE;
                        $_SESSION['failed'] = FALSE;
			redirect(base_url('emp_medical_cont/getEmpMedicalById/'.$this->uri->segment(3)));
		
		}
		else{
			$_SESSION['msg']="Failed To Add Record";
			$_SESSION['success_add'] = FALSE;
                        $_SESSION['failed_add'] = TRUE;
                        redirect(base_url('emp_medical_cont/getEmpMedicalById/'.$this->uri->segment(3)));
		}
            }
            else
            {
                $data['records']=$this->emp_model->tableGetEmpById();
                $data['main_page'] = "medical_add";
		$this->load->view('includes/page2',$data);
            }
	}
	function updateMedical(){
            if($this->input->post('submit')){
		$data=array(
		'cat'=>$this->input->post('cat'),
		'admittedcmh'=>$this->input->post('cmh'),
		'from'=>$this->input->post('from'),
		'to'=>$this->input->post('to'),
		'partiio'=>$this->input->post('partiio'),
		'diseases'=>$this->input->post('diseases'),
		'disposal'=>$this->input->post('disposal'),
		
		'remarks'=>$this->input->post('remarks'),
                    'review'=>$this->input->post('review'),
		
		);
		if($this->emp_model->updateTableRecord('medicalrecord',$data)){
			$_SESSION['msg']="Record Updated Successfully";
                        $_SESSION['success']=TRUE;
                        $_SESSION['failure']=FALSE;
                        redirect(base_url('emp_medical_cont/getEmpMedicalById/'.$this->uri->segment(3)));
			//$this->load->view('medical',$data);
		
		}
		else{
			$_SESSION['msg']="Failed To Update Record";
                        $_SESSION['success']=FALSE;
                        $_SESSION['failure']=TRUE;
                        redirect(base_url('emp_medical_cont/getEmpMedicalById/'.$this->uri->segment(3)));
			//$this->load->view('medical',$data);
		}
            }
            else
            {
                $data['records']=$this->emp_model->tableGetEmpById();
                $data['meds']=$this->emp_model->tableGetById('medicalrecord');
                $data['main_page'] = "medical_edit";
		$this->load->view('includes/page2',$data);
            }
	
	
	}
	function deleteMedical(){
		if($this->emp_model->deleteTableRecord('medicalrecord')){
			$_SESSION['success']=TRUE;
                        $_SESSION['failure']=FALSE;
                        $_SESSION['msg']="Record Deleted Successfully";
                        redirect(base_url('emp_medical_cont/getEmpMedicalById/'.$this->uri->segment(3)));
		
		}
		else{
                        $_SESSION['success_del']=FALSE;
                        $_SESSION['failure_del']=TRUE;
			$_SESSION['msg']="Failed To Delete Record";
			redirect(base_url('emp_medical_cont/getEmpMedicalById/'.$this->uri->segment(3)));
		
		}
		
	
	
	
	}
        
        function load_deleted_medical(){
    	$data['records']=$this->emp_model->tableGetEmpById();
            $data['meds']=$this->emp_model->getdeleted('medicalrecord');
            $data['main_page'] = "medical";
            $data['trash']=TRUE;
            $this->load->view('includes/page2',$data);
    	}
    
    function recover(){
        $data=array('status'=>1);
    	if($this->emp_model->updateTableRecord('medicalrecord',$data))
        {
            $_SESSION['success']=TRUE;
            $_SESSION['failure']=FALSE;
            $_SESSION['msg']="Record Recovered Successfully";
            redirect(base_url('emp_medical_cont/load_deleted_medical/'.$this->uri->segment(3)));
        }
        else
        {
            $_SESSION['success']=FALSE;
            $_SESSION['failure']=TRUE;
            $_SESSION['msg']="Failed To Recover Record";
            redirect(base_url('emp_medical_cont/load_deleted_medical/'.$this->uri->segment(3)));
        }
    
    
    
    }


}